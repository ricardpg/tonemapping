/*
   Autor(s)      : Jordi Ferrer Plana
   e-mail        : jferrerp@eia.udg.edu
   Branch        : Computer Vision

   Working Group : Underwater Vision Lab
   Project       : Matlab Wrapper to ToneMapping algorithm.
   Homepage      : http://uvl.udg.edu

   Module        : Tone Mapping Interface.

   File          : tonemappingMx.cpp
   Date          : 10/09/2008 - 19/09/2008
   Encoding      : ISO-8859-1 (Latin-1)

   Compiler      : MATLAB >= 7.0 & g++ >= 4.x
   Libraries     : 

   Notes         :

  -----------------------------------------------------------------------------

   Copyright (C) 2008 by Jordi Ferrer Plana

   This source code is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This source code is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

   See GNU licenses at http://www.gnu.org/licenses/licenses.html for
   more details.

  -----------------------------------------------------------------------------
*/

// Select whether to Show Debug Output or not
// #define __DEBUG_OUTPUT__

#include <mex.h>                       // Matlab: Mex Functions

#ifdef __DEBUG_OUTPUT__
  #include <iostream>                  // cerr
#endif

#include <errorMx.h>                   // Improved Error output to Matlab's Console
#include <histogram.h>
#include <matrix.h>

// Number of Fields in Parameters Structure
#define PARAM_NUM_FIELDS 8

typedef struct SParamFields
{
  char *FieldName;
  double DefaultValue;
  double *VarRef;
} TParamFields;

// Parameters Field Names
TParamFields ParamFields[PARAM_NUM_FIELDS] = { { "InputBrightness", 1.0, &INPUT_BRIGHTEST },
                                               { "InputMediaContrast", 1e20, &INPUT_MEDIA_CONTRAST },
                                               { "ContrastClippingRatioLower", 0.0, &CONTRAST_CLIPPING_OF_AREA_RATIO_LOWER },
                                               { "ContrastClippingRatioUpper", 0.0, &CONTRAST_CLIPPING_OF_AREA_RATIO_UPPER },
                                               { "OutputBrightness", 1.0, &OUTPUT_BRIGHTEST },
                                               { "OutputMediaContrast", 50, &OUTPUT_MEDIA_CONTRAST },
                                               { "MaxContrastLimitUpperPerStep", 1.25, &MAX_CONTRAST_LIMIT_UPPER_PER_STEP },
                                               { "MinContrastLimitUpperPerStep", 1.0 / 1.25, &MIN_CONTRAST_LIMIT_UPPER_PER_STEP } };
                                               
// Identify the input parameters by it's index
enum { I=0, PARAMSTRUCT, SEQWEIGHT, SEQUPPERLIMIT, SEQLOWERLIMIT };
// Identify the output parameters by it's index
enum { J=0 };

// No defined as public in include <histogram.h>
void amclahe ( matrix<double> & pic );
void clipAndSet ( matrix<double> & pic );
void file_to_amclahe ( matrix<double> & picin );
void amclahe_to_file ( matrix<double> & picin );

// This is done while reading in the original code, here we convert specificly
void convertMatrix2RGBImage ( matrix<double> &GrayImage, matrix< vector3<double> > &RGBImage,
                              double *SrcImage, unsigned int Width, unsigned int Height );
void convertMatrix2GrayImage ( matrix<double> &GrayImage, double *Image, unsigned int Width, unsigned int Height );
// This is done while writting in the original code, here we convert specificly
void convertRGBImage2Matrix ( matrix<double> &GrayImage, matrix< vector3<double> > &RGBImage, double *DstImage );
void convertGrayImage2Matrix ( matrix<double> &GrayImage, double *DstImage );

#ifdef __DEBUG_OUTPUT__
  // Debugging Purpouses
  void writeWeightSequences ( void );
#endif

// Mex Function
//
void mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  // Input Parameter Checks
  if ( nlhs != 1 || ( nrhs != 4 && nrhs != 5 ) )
  {
    printfmexErrMsgTxt ( "Usage: J = tonemappingMx ( I, ParamStruct, SeqWeight, SeqUpperLimit [, SeqLowerLimit ] )\n"
                         "       ParamStruct Fields (all double):\n"
                         "         .%s [ = %e ]\n"
                         "         .%s [ = %e ]\n"
                         "         .%s [ = %e ]\n"
                         "         .%s [ = %e ]\n"
                         "         .%s [ = %e ]\n"
                         "         .%s [ = %e ]\n"
                         "         .%s [ = %e ]\n"
                         "         .%s [ = %e ]\n",
                         ParamFields[0].FieldName, ParamFields[0].DefaultValue,
                         ParamFields[1].FieldName, ParamFields[1].DefaultValue,
                         ParamFields[2].FieldName, ParamFields[2].DefaultValue,
                         ParamFields[3].FieldName, ParamFields[3].DefaultValue,
                         ParamFields[4].FieldName, ParamFields[4].DefaultValue,
                         ParamFields[5].FieldName, ParamFields[5].DefaultValue,
                         ParamFields[6].FieldName, ParamFields[6].DefaultValue,
                         ParamFields[7].FieldName, ParamFields[7].DefaultValue );
  }

  if ( !mxIsDouble ( prhs[I] ) )
    printfmexErrMsgTxt ( "Input image I (type %d) must be double!\n", mxGetClassID ( prhs[I] ) );

  // Get the Dimensions
  unsigned int Width, Height, Depth;

  const int *Dimensions = mxGetDimensions ( prhs[I] );

  Height = Dimensions[0];
  if ( mxGetNumberOfDimensions ( prhs[I] ) == 3 )
  {
    Width = Dimensions[1];
    Depth = Dimensions[2];
  }
  else
    if ( mxGetNumberOfDimensions ( prhs[I] ) == 2 )
    {
      Width = Dimensions[1];
      Depth = 1;
    }
    else
      Depth = 0;

  // Dimensions
  if ( Depth != 1 && Depth != 3 )
    printfmexErrMsgTxt ( "I (of %d dimensions) must be a bidimensional (h,w) graylevel image matrix or a tridimensional (h,w,d) RGB image matrix!\n",
                         mxGetNumberOfDimensions ( prhs[I] ) );

#ifdef __DEBUG_OUTPUT__
  std::cerr << "Input Image Size: " << Width << "x" << Height << "x" << Depth << std::endl;
#endif

#ifdef __DEBUG_OUTPUT__
  std::cerr << "Reading weights!" << std::endl;
#endif

  // Loops
  unsigned int i, j;

  // Weight and Limit sizes for Sequence Weights
  unsigned int Wm = mxGetM ( prhs[SEQWEIGHT] );
  unsigned int Wn = mxGetN ( prhs[SEQWEIGHT] );

  if ( !mxIsDouble ( prhs[SEQWEIGHT] ) || Wm != 1 || Wn < 1 )
    printfmexErrMsgTxt ( "SeqWeight (%dx%d) (type %d) must be a 1xn with n > 0 vector of double weights!\n",
                         Wm, Wn, mxGetClassID ( prhs[SEQWEIGHT] ) );

  if ( !mxIsCell ( prhs[SEQUPPERLIMIT] ) )
    printfmexErrMsgTxt ( "SeqUpperLimit must be a cell array!\n" );

  if ( mxGetM ( prhs[SEQUPPERLIMIT] ) != Wm || mxGetN ( prhs[SEQUPPERLIMIT] ) != Wn )
    printfmexErrMsgTxt ( "SeqWeight (%dx%d) and SeqUpperLimit (%dx%d) must share the same dimensions (1xn)!\n",
                         mxGetM ( prhs[SEQUPPERLIMIT] ), mxGetN ( prhs[SEQUPPERLIMIT] ), Wm, Wn );

  if ( nrhs > 4 )
  {
    if ( !mxIsCell ( prhs[SEQLOWERLIMIT] ) )
      printfmexErrMsgTxt ( "SeqLowerLimit if provided must be a cell array!\n" );

    if ( !mxIsEmpty ( prhs[SEQLOWERLIMIT] ) )
    {
      if ( mxGetNumberOfDimensions ( prhs[SEQUPPERLIMIT] ) != mxGetNumberOfDimensions ( prhs[SEQLOWERLIMIT] ) )
        printfmexErrMsgTxt ( "SeqUpperLimit (dimensions %d) and SeqLowerLimit (dimensions %d) if specified must share the same number of dimensions!\n",
                             mxGetNumberOfDimensions ( prhs[SEQUPPERLIMIT] ), mxGetNumberOfDimensions ( prhs[SEQLOWERLIMIT] ) );

      if ( mxGetM ( prhs[SEQLOWERLIMIT] ) != Wm || mxGetN ( prhs[SEQLOWERLIMIT] ) != Wn )
        printfmexErrMsgTxt ( "SeqUpperLimit (%dx%d) and SeqLowerLimit (%dx%d) if specified must be cell arrays of 1x%d elements!\n",
                             Wm, Wn, mxGetM ( prhs[SEQLOWERLIMIT] ), mxGetN ( prhs[SEQLOWERLIMIT] ), Wn );
    }
  }

#ifdef __DEBUG_OUTPUT__
  std::cerr << "Setting Up Global Parameters!"<< std::endl;
#endif
  
  // Default Values

  // Field Name / Variable                   //  Data Type   // Default Value      // Notes
  // ================================================================================================================================================================================
  // INPUT_FILENAME                          // char *       //  None              // Raw image input filename. Just for read raw image file. Not Used.
  // HEADER_LENGTH                           // unsigned int //  0                 // Raw image header to skip. Just for read raw image file. Not Used.
  // IMAGE_SIZE_X = Width;                      // unsigned int //  None           // Raw image width. Just for read raw image file. Not Used.
  // IMAGE_SIZE_Y = Height;                     // unsigned int //  0              // Raw image height. Just for read raw image file. Not Used.
  // CHANNEL_TYPE                            // char *       //  None              // Can be INT8, INT16, INT32, FLOAT.
  // IMAGE_TYPE                              // char *       //  None              // Can be RGB_I, RGB_NI, GRAY.
     // Two parameters above define the following variable:
     // imageType                            // imagetype    //  None              // Just for read raw image file. Not Used.
                                             //       enum imagetype {rgbbyte, rgbshort, rgblong, rgbfloat, rgbnibyte, rgbnishort, rgbnilong, rgbnifloat, graybyte, grayshort, graylong, grayfloat };
  // ENDIAN                                  // char *       //  None              // Can be LITTLE, BIG.
     // Defines the variable:
     // imageBE                              // boolean      //  None              // Raw image endian. Just for read raw image file. Not Used.
  // ROW_END_DUMMY_BYTES_NUMBER_MOD          // unsigned int //  0                 // Raw image row alignment in image type measure. Just for read raw image file. Not Used.
     // Defines the variable:
     // ROW_END_DUMMY_BYTES_NUMBER           // unsigned int //  0                 // Raw image row alignment in bytes. Just for read raw image file. Not Used.
  INPUT_BRIGHTEST = 1.0;                     // double       // -1 or 1 (float)    // Depends on image type, maximum value for brightness.
  INPUT_MEDIA_CONTRAST = 1e20;               // double       // -1 or 1e20 (float) // Contrats del dispositiu de sortida.
  CONTRAST_CLIPPING_OF_AREA_RATIO_LOWER = 0; // double       // 0                  // Lower Clipping. I DON'T KNOW what is doing.
  CONTRAST_CLIPPING_OF_AREA_RATIO_UPPER = 0; // double       // 0                  // Upper Clipping. I DON'T KNOW what is doing.
  OUTPUT_BRIGHTEST = 1.0;                    // double       // -1 or 1 (float)    //
  OUTPUT_MEDIA_CONTRAST = 50;                // double       // 50                 // Contrast del dispositiu de sortida.
  MAX_CONTRAST_LIMIT_UPPER_PER_STEP = 1.25;  // double       // 1.e16              // Maximum Contrast per step. I DON'T KNOW what is doing.
  MIN_CONTRAST_LIMIT_UPPER_PER_STEP =        // double       // 1 / MAX_CONTRAST_..// Minimum Contrast per step. I DON'T KNOW what is doing.
    1.0 / MAX_CONTRAST_LIMIT_UPPER_PER_STEP;
  // OUTPUT_FILENAME                         // char *       // None               // Raw image output filename. Just for read raw image file. Not Used.

  // The Folloing sequence can be repeated several times. The SEQUENCE_WEIGHTS should sum 1.0
  // SEQUENCE_WEIGHT                         // double[]     // None               // Weight [0-1] for the sequence.
  // HISTLIMIT_UPPER                         // double[][]   // None               // Upper Histogram Limit. Ex. 1.6  1.5   1.4   1.1    1.   1.   1.   1.
  // HISTLIMIT_LOWER                         // double[][]   // None               // Lower Histogram Limit. Ex. 1.6  1.5   1.4   1.1    1.   1.   1.   1.
     // The sequence of the three above parameters define the following variables:
     // sequenceWeight, sequenceUpperLimit, sequenceLowerLimit

  if ( mxIsStruct ( prhs[PARAMSTRUCT] ) )
  {
    int FieldNum;

#ifdef __DEBUG_OUTPUT__
    std::cerr << "Reading Parameter Fields:"<< std::endl;
#endif    

    for ( i = 0; i < PARAM_NUM_FIELDS; i++ )
    {      
#ifdef __DEBUG_OUTPUT__
      std::cerr << "  ." << ParamFields[i].FieldName;
#endif

      if ( ( FieldNum = mxGetFieldNumber ( prhs[PARAMSTRUCT], ParamFields[i].FieldName ) ) >= 0 )
        if ( mxIsDouble ( mxGetFieldByNumber ( prhs[PARAMSTRUCT], 0, FieldNum ) ) &&
             mxGetM ( mxGetFieldByNumber ( prhs[PARAMSTRUCT], 0, FieldNum ) ) == 1 &&
             mxGetN ( mxGetFieldByNumber ( prhs[PARAMSTRUCT], 0, FieldNum ) ) == 1 )
        {
          *(ParamFields[i].VarRef) = mxGetScalar ( mxGetFieldByNumber ( prhs[PARAMSTRUCT], 0, FieldNum ) );
#ifdef __DEBUG_OUTPUT__
          std::cerr << " (" << FieldNum << ") = " << *(ParamFields[i].VarRef);
#endif
        }
#ifdef __DEBUG_OUTPUT__
        else
        {
          std::cerr << " (" << FieldNum << ") = " << *(ParamFields[i].VarRef) << "(Default)";
        }
#endif

#ifdef __DEBUG_OUTPUT__
        std::cerr << std::endl;
#endif
    }
  }
  else
    printfmexErrMsgTxt ( "ParamStruct must be a structure!\n" );
    

#ifdef __DEBUG_OUTPUT__
  std::cerr << "Filling Sequence Weights, Upper and Lower limit structures" << std::endl;
#endif

  // Allocate Space for Weights
  sequenceWeight.erase ( );
  sequenceWeight.resize ( Wn );

  sequenceUpperLimit.erase ( );
  sequenceUpperLimit.resize ( Wn );

  sequenceLowerLimit.erase ( );
  sequenceLowerLimit.resize ( Wn );

  double *PrWeight = mxGetPr ( prhs[SEQWEIGHT] );

  // Sum the weights to Normalize
  double w = 0.0;
  for ( i = 0; i < Wn; i++ )
    w += PrWeight[i];

  mxArray *upperLimit, *lowerLimit;
  double *PrUpper, *PrLower;

  for ( i = 0; i < Wn; i++ )
  {
    sequenceWeight(i) = PrWeight[i] / w;

    if ( ( upperLimit = mxGetCell ( prhs[SEQUPPERLIMIT], i ) ) != NULL )
    {
      unsigned int Ur = mxGetM ( upperLimit );
      unsigned int Uc = mxGetN ( upperLimit );
      unsigned int k;

      PrLower = NULL;
      if ( !mxIsDouble ( upperLimit ) || Ur <= 0 || Uc <= 0 )
        printfmexErrMsgTxt ( "Cell %d of SeqUpperLimit (%dx%d) must be a non-empty double matrix!\n",
                             i, Ur, Uc );
      else
        if ( nrhs > 4 )
        {
          if ( !mxIsEmpty ( prhs[SEQLOWERLIMIT] ) )
          {
            if ( ( lowerLimit = mxGetCell ( prhs[SEQLOWERLIMIT], i ) ) != NULL )
            {
              unsigned int Lr = mxGetM ( lowerLimit );
              unsigned int Lc = mxGetN ( lowerLimit );

              if ( !mxIsDouble ( lowerLimit ) || Ur != Lr || Uc != Lc )
                printfmexErrMsgTxt ( "Cell %d of SeqLowerLimit (%dx%d) if provided must share the same dimensions than cell %d of SeqUpperLimit (%dx%d)!\n",
                                      i, Ur, Uc, i, Lr, Lc );
              else
                // Get pointer to LowerLimit
                PrLower = mxGetPr ( lowerLimit );
            }
            else
              printfmexErrMsgTxt ( "Null Cell %d in SeqLowerLimit (%dx%d)!\n", i, Wm, Wn );
          }
        }

        // Get pointer to UpperLimit
        PrUpper = mxGetPr ( upperLimit );

        // Store values in structures
        sequenceUpperLimit(i).resize ( Ur );
        sequenceLowerLimit(i).resize ( Ur );
        for ( j = 0; j < Ur; j++ )
        {
          sequenceUpperLimit(i)(j).resize ( Uc );
          sequenceLowerLimit(i)(j).resize ( Uc );
          for ( k = 0; k < Uc; k++ )
          {
            sequenceUpperLimit(i)(j)(k) = PrUpper[ k * Ur + j ];
            if ( PrLower != NULL )
              sequenceLowerLimit(i)(j)(k) = PrLower[ k * Ur + j];
            else
              sequenceLowerLimit(i)(j)(k) = 1.0 / sequenceUpperLimit(i)(j)(k);
          }
        }
    }
    else
      printfmexErrMsgTxt ( "Null Cell %d in SeqUpperLimit (%dx%d)!\n", i, Wm, Wn );
  }

#ifdef __DEBUG_OUTPUT__
  writeWeightSequences ( );
#endif


#ifdef __DEBUG_OUTPUT__
  std::cerr << "Copy Input Matrix to Image Object!"<< std::endl;
#endif

  matrix< vector3<double> > ImageRGBDouble;

  double *PtrI = mxGetPr ( prhs[I] );

  if ( Depth == 1 )
    convertMatrix2GrayImage ( grayImage, PtrI, Width, Height );
  else
    convertMatrix2RGBImage ( grayImage, ImageRGBDouble, PtrI, Width, Height );


#ifdef __DEBUG_OUTPUT__
  std::cerr << "Applying Clippings!"<< std::endl;
#endif
  
  clipAndSet ( grayImage );

#ifdef __DEBUG_OUTPUT__
  std::cerr << "Convert from Image to Log Scale for amclahe!"<< std::endl;
#endif

  file_to_amclahe ( grayImage );

#ifdef __DEBUG_OUTPUT__
  std::cerr << "Apply amclahe!"<< std::endl;
#endif

  amclahe ( grayImage );

#ifdef __DEBUG_OUTPUT__
  std::cerr << "Convert Log Scale from amclahe to Image!"<< std::endl;
#endif

  amclahe_to_file ( grayImage );

#ifdef __DEBUG_OUTPUT__
  std::cerr << "Copy Image Object to Output Matrix!"<< std::endl;
#endif

  // Convert back to Matlab Matrix
  if ( Depth == 1 )
  {
    // Create Output Image
    plhs[J] = mxCreateDoubleMatrix ( Height, Width, mxREAL );
    double *PtrJ = mxGetPr ( plhs[J] );

    convertGrayImage2Matrix ( grayImage, PtrJ );
  }
  else
  {
    // Create Output Image
    const mwSize Dims[3] = { Height, Width, Depth };

    if ( ( plhs[J] = mxCreateNumericArray ( 3, Dims, mxDOUBLE_CLASS, mxREAL ) ) != NULL )
    {
      double *PtrJ = mxGetPr ( plhs[J] );
      convertRGBImage2Matrix ( grayImage, ImageRGBDouble, PtrJ );
    }
    else
      printfmexErrMsgTxt ( "Error creating output RGB Matrix (%dx%dx%d)!\n", Height, Width, Depth );
  }
}


// From Source to Gray Image
void convertMatrix2GrayImage ( matrix<double> &GrayImage, double *Image, unsigned int Width, unsigned int Height )
{
  double brightest = static_cast<double> ( INPUT_BRIGHTEST );
  double darkest = static_cast<double> ( INPUT_BRIGHTEST ) / static_cast<double> ( INPUT_MEDIA_CONTRAST );

  // Allocate Space for Color Image
  GrayImage.resize ( Width, Height );
  // Copy Data
  for ( unsigned int i = 0; i < Width; i++ )
    for ( unsigned int j = 0; j < Height; j++ )
      GrayImage(i, j) = max ( min ( *Image++, brightest ) , darkest );
}

// From Source to Gray and Color Image
void convertMatrix2RGBImage ( matrix<double> &GrayImage, matrix< vector3<double> > &RGBImage,
                              double *SrcImage, unsigned int Width, unsigned int Height )
{
  double brightest = static_cast<double> ( INPUT_BRIGHTEST );
  double darkest = static_cast<double> ( INPUT_BRIGHTEST ) / static_cast<double> ( INPUT_MEDIA_CONTRAST );

  // Offset of a Plane
  unsigned int PlaneOffset = Width * Height;

  // Allocate Space for Color Image
  RGBImage.resize ( Width, Height );
  GrayImage.resize ( Width, Height );
  // Copy Data
  for ( unsigned int i = 0; i < Width; i++ )
    for ( unsigned int j = 0; j < Height; j++ )
    {
      vector3<double> &v = RGBImage(i, j);
      v.x = *SrcImage;
      v.y = *(SrcImage + PlaneOffset);
      v.z = *(SrcImage + 2 * PlaneOffset);
      SrcImage++;
      
      v.x = max ( min ( v.x, brightest ), darkest );
      v.y = max ( min ( v.y, brightest ), darkest );
      v.z = max ( min ( v.z, brightest ), darkest );
      GrayImage(i, j) = color_to_gray ( v );
      RGBImage(i, j) = v;
    }
}

void convertGrayImage2Matrix ( matrix<double> &GrayImage, double *DstImage )
{
  // Copy Data
  for ( unsigned int i = 0; i < GrayImage.getsizex ( ); i++ )
    for ( unsigned int j = 0; j < GrayImage.getsizey ( ); j++ )
      *DstImage++ = GrayImage(i, j);
}

void convertRGBImage2Matrix ( matrix<double> &GrayImage, matrix< vector3<double> > &RGBImage, double *DstImage )
{
  vector3<double> V;

  // Offset of a Plane
  unsigned int PlaneOffset = GrayImage.getsizex ( ) * GrayImage.getsizey ( );

  // Copy Data
  for ( unsigned int i = 0; i < GrayImage.getsizex ( ); i++ )
    for ( unsigned int j = 0; j < GrayImage.getsizey ( ); j++ )
    {
      V = gray_to_color ( RGBImage(i, j), GrayImage(i, j) );
      *DstImage = V.x;
      *(DstImage + PlaneOffset) = V.y;
      *(DstImage + 2 * PlaneOffset) = V.z;
      DstImage++;      
    }
}

#ifdef __DEBUG_OUTPUT__
  void writeWeightSequences ( void )
  {
    std::cerr << "WEIGHT Size: " << sequenceWeight.getsize ( ) << std::endl;
    for ( unsigned i = 0; i < sequenceWeight.getsize ( ); i++ )
      std::cerr << "  Val " << i << ": " << sequenceWeight(i) << std::endl;

    std::cerr << "upperLimit Size: " << sequenceUpperLimit.getsize ( ) << std::endl;
    std::cerr << "lowerLimit Size: " << sequenceLowerLimit.getsize ( ) << std::endl;
    for ( unsigned i = 0; i < sequenceUpperLimit.getsize ( ); i++ )
    {
      std::cerr << "upperLimit(" << i << ") Size: " << sequenceUpperLimit(i).getsize ( ) << std::endl;
      std::cerr << "lowerLimit(" << i << ") Size: " << sequenceLowerLimit(i).getsize ( ) << std::endl;
      for ( unsigned j = 0; j < sequenceUpperLimit(i).getsize ( ); j++ )
      {
        std::cerr << "  upperLimit(" << i << ")(" << j << ") Size: " << sequenceUpperLimit(i)(j).getsize ( ) << std::endl;
        std::cerr << "  lowerLimit(" << i << ")(" << j << ") Size: " << sequenceLowerLimit(i)(j).getsize ( ) << std::endl;
        for ( unsigned int k = 0; k < sequenceUpperLimit(i)(j).getsize ( ); k++ )
          std::cerr << "    Values(" << i << ")(" << j << ")(" << k << ") = ("
                    << sequenceLowerLimit(i)(j)(k) << ", " << sequenceUpperLimit(i)(j)(k) << ")" << std::endl;
      }
    }
  }
#endif
