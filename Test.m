%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Tone - Mapping Testing Functions.
%
%  File          : Test.m
%  Date          : 19/02/2007 - 11/12/2008
%  Encoding      : ISO-8859-1 (Latin-1)
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : 
%
%  Notes         :
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2008 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%

% Clear
%
disp ( 'Clearing...' );
clc; clear;

disp ( 'Setting up the parameters...' );
% Configuration Parameters
%
% Show Source and Resulting Image
DoShowSrcImage = false;
DoShowDstImage = false;
% Calculate or not the Lower Limit
DoLowerLimit = false;
% Compute Input Gamma Correction
DoInputGamma = false;
  InputGamma = 2.2;
% Compute Output Gamma Correction
DoOutputGamma = true;
  OutputGamma = 2.2;
% Number of Input Image Bits
NumInputBits = 14;

DoApplyMask = false;
  HistUpperClipPercentage = 98;
  Nw = 8;
  Nh = 8;

% RGB tones for colorizing final image
DoTransferSepiaGreenTones = false;
  ITone = [ 1.0; 0.7; 0.5 ];
  BTone = [ 0.0; 0.8; 0.4 ];
%  BTone = [ 0.0; 0.4; 0.8 ];

% For Radial Correction
DoRadiometricCorrection  = 1;
%  RadiometricCorrectCoefFilePath = '/mnt/uvl/DataSets/MoMAR08-LuckyStrike/OTUS/Corrected';
%  RadiometricCorrectCoefFilePath = '/mnt/uvl/DataSets/Momareto06/OTUS/Original';
  RadiometricCorrectCoefFilePath = '/mnt/uvl/DataSets/MoMAR08-Rainbow/OTUS/Corrected';
  RadiometricCorrectCoefFileName = 'CorrectionCoeff.mat';
  RadiometricCorrectCoefFile = [ RadiometricCorrectCoefFilePath '/' RadiometricCorrectCoefFileName ];

DoSaveDstImage = true;
% SrcImagePath = '/mnt/uvl/DataSets/MoMAR08-LuckyStrike/OTUS/Mirrored/Images';
% SrcImagePath = '/mnt/uvl/DataSets/Momareto06/OTUS/Original/Images';
SrcImagePath = '/mnt/uvl/DataSets/MoMAR08-Rainbow/OTUS/Mirrored/Images';
SrcImageExt = 'tif';
% DstImagePath = '/home/ftp/ToJavier';
% DstImagePath = '/mnt/uvl/DataSets/MoMAR08-LuckyStrike/OTUS/ToneMapping/Images';
% DstImagePath = '/mnt/uvl/DataSets/Momareto06/OTUS/ToneMapping/Images';
DstImagePath = '/mnt/uvl/DataSets/MoMAR08-Rainbow/OTUS/ToneMapping/Images';
DstImageExt = 'png';

% ImgFolderContentFile = 'MirroredImgFolderContent.mat';
ImgFolderContentFile = 'OriginalImgFolderContent.mat';

% Check Directory Content
%
if exist ( ImgFolderContentFile, 'file' ) && ~exist ( 'ImgFolderContent', 'var' );
  disp ( sprintf ( 'Load folder content from file "%s"...', ImgFolderContentFile ) );
  load ( ImgFolderContentFile );
  % If the variable was not loaded, generate it
  if ~exist ( 'ImgFolderContent', 'var' );
    DoGenerateFolderContent = true;
  else
    DoGenerateFolderContent = false;
  end
else
  DoGenerateFolderContent = ~exist ( 'ImgFolderContent', 'var' );    
end

% Save Directory Content
%
if DoGenerateFolderContent;
  disp ( sprintf ( 'Generate folder "%s" content...', SrcImagePath ) );
  % Just Listing the whole folder
  ImgFolderContent = dir ( [ SrcImagePath '/*.' SrcImageExt ] );
  % Save Folder Content
  save ( ImgFolderContentFile, 'ImgFolderContent' );
end

% Images To Test
FirstImage = 1;
LastImage = max ( size ( ImgFolderContent ) );
%LastImage = 120;

%Parameters.InputBrightness = 58000;
Parameters.InputBrightness = 0.9;
%Parameters.InputMediaContrast = 1e20;
Parameters.InputMediaContrast = 70;
Parameters.ContrastClippingRatioLower = 0.0;
Parameters.ContrastClippingRatioUpper = 0.0;
Parameters.OutputBrightness = 1.0;
Parameters.OutputMediaContrast = 60.0;
Parameters.MaxContrastLimitUpperPerStep = 1.2500;
Parameters.MinContrastLimitUpperPerStep = 1 / Parameters.MaxContrastLimitUpperPerStep;

%SeqWeights = [ 0.9 0.1 ];
SeqWeights = 1;

%SeqUpperLimit = cell ( 1, 2 );
%SeqUpperLimit{1} = [ 1.6 1.5 1.4 1.1 1.0 1.0 1.0 1.0; ...
%                     1.2 1.2 1.2 1.1 1.0 1.0 1.0 1.0; ...
%                     1.4 1.3 1.2 1.0 1.0 1.0 1.0 1.0 ];
SeqUpperLimit = cell ( 1, 1 );
SeqUpperLimit{1} = [ 1.6 1.5 1.4 1.1 1.0 1.0 1.0 1.0; ...
                     1.4 1.3 1.2 1.1 1.1 1.0 1.0 1.0;
                     1.11 1.1 1.05 1.0 1.0 1.0 0.9 0.8];

%SeqUpperLimit{2} = [ 1.0 1.0 1.0 1.0 1.0 1.5 1.8 4.0 ];

if DoLowerLimit;
  SeqLowerLimit = cell ( size ( SeqUpperLimit, 1 ), size ( SeqUpperLimit, 2 ) );
  for i = 1 : 2; 
    SeqLowerLimit{i} = 1 ./ ( SeqUpperLimit{i} .* SeqUpperLimit{i} );
  end
else
  SeqLowerLimit = {};
end

% Compute Filenames
disp ( sprintf ( 'Compute the %d filenames...', size ( ImgFolderContent, 1 ) ) );
SrcImageList = cell ( size ( ImgFolderContent, 1 ), 1 );
for i = 1 : size ( ImgFolderContent, 1 );
  SrcImageList{i} = ImgFolderContent(i).name(1:size(ImgFolderContent(i).name, 2) - 4);
end

% Get Initial Time
IterationInitTime = cputime;
InitTime = cputime;

% Load Radial Correction Coefficient
if DoRadiometricCorrection  && exist ( RadiometricCorrectCoefFile, 'file' );
  disp ( sprintf ( 'Load Radial Correction Coefficient from file "%s"...', RadiometricCorrectCoefFile ) );
  load ( RadiometricCorrectCoefFile );
end

NumImages = LastImage - FirstImage + 1;
for Iteration = 1 : NumImages;
  CurrentImage = FirstImage + Iteration - 1;

  disp ( sprintf ( 'Processing Step %d / %d...', Iteration, NumImages ) );

  % Do the job
  SrcImageFilePath = [ SrcImagePath '/' SrcImageList{CurrentImage} '.' SrcImageExt ];

  disp ( sprintf ( '  Loading image ''%s''...', SrcImageFilePath ) );
  I = imread ( SrcImageFilePath );
  Width = size ( I, 2 );
  Height = size ( I, 1 );

  % Check HDR
  if strcmpi ( class ( I ), 'uint8' ) || strcmpi ( class ( I ), 'int8' );
    error ( 'MATLAB:Test:Input', ...
            'Input data type (%s) must allow HDR, so must be diferent of uint8 and int8!\n', class ( I ) );
  end

  if DoApplyMask;
    % Compute Mask For White particles
    Mask = zeros ( Height, Width );
    WinW = Width / Nw;
    WinH = Height / Nh;
    if WinW ~= fix ( WinW ) || WinH ~= fix ( WinH );
      error ( 'MATLAB:Test:Input', ...
              'The number of windows (%f, %f) must be divisible by the image size (%f, %f)!', Nw, Nh, Width, Height );
    end

    % Number of sigmas for a given percentage
    NSigmas = (erfinv(HistUpperClipPercentage/100.0) * sqrt(2));

    J = zeros ( Height, Width );
    for i = 1 : Nw;
      Ol = fix ( (i-1) * WinW + 1 );
      Or = fix ( Ol + WinW - 1 );
      for j = 1 : Nw;
        Il = fix ( (j-1) * WinH + 1 );
        Ir = fix ( Il + WinH - 1 );
      
        W = I(Il:Ir,Ol:Or);
        Min = min ( W(:) );
        Max = max ( W(:) );

%        MeanW = mean ( W(:) );
%        StdW = std ( double ( W(:) ) );
        MLEs = mle ( double(W(:)), 'distribution', 'normal' ); 
        MeanW = MLEs(1);
        StdW = MLEs(2);
        MaskW = abs ( ( W - MeanW ) ) <= NSigmas * StdW;

        Idx = find ( MaskW == 0 );
        W(Idx) = MeanW;

        J(Il:Ir,Ol:Or) = W;
        Mask(Il:Ir,Ol:Or) = MaskW;
      end
    end
  else
    J = I;
    Mask = ones ( Height, Width );
  end

  % Adjust to the Real Dynamic Range
  K = double ( J ) / double ( 2 ^ NumInputBits );

  % Convert to double
  if ( DoInputGamma )
    % Gamma Correction
    disp ( '  Applying Input Gamma Correction...' );
    L = K .^ InputGamma;
  else
    % Linear Values
    L = K;
  end

  % Create the Coeff if it does not exist
  if DoRadiometricCorrection  && ~exist ( 'CorrectionCoeff', 'var' );
    CorrectionCoeff = ones ( Height, Width );
  end

  if DoRadiometricCorrection;
    disp ( '  Applying Non-linear Radiometric Correction...' );
    Li = L .* CorrectionCoeff;
  else
    Li = L;
  end
  
  % Do the Job
  disp ( '  Applying Tone Mapping algorithm...' );
  M = tonemappingMx ( Li, Parameters, SeqWeights, SeqUpperLimit, SeqLowerLimit );

  if DoTransferSepiaGreenTones;
    disp ( '  Transfer sepia/green tones...' );
    % Background
    Ib = double ( I ) / double ( 2 ^ NumInputBits );
    N = zeros ( size ( M, 1 ), size ( M, 2 ), 3 );
    for i = 1 : 3;
      N(:,:,i) = M * ITone(i) .* double(Mask) + Ib.^(1/OutputGamma) .* BTone(i) .* double(~Mask);
    end
  else
    N = M;
  end

  % Convert to double
  if DoOutputGamma;
    % Gamma Correction
    disp ( ' Applying Output Gamma Correction...' );
    O = N .^ (1 / OutputGamma);
  else
    % Linear Values
    O = N;
  end

  if DoSaveDstImage;
    DstImageFilePath = [ DstImagePath '/' SrcImageList{CurrentImage} '.' DstImageExt ];
    disp ( sprintf ( '  Saving resulting image "%s"...', DstImageFilePath ) );
    P = uint8 ( O * 255 );
    imwrite ( P, DstImageFilePath );
  end

  % Show Results
  if DoShowSrcImage;
    disp ( '  Show source image...' );
    figure;
    imshow ( K );
    s = [ 'Source Image ' SrcImageList{CurrentImage} ];
    title ( s ); set ( gcf, 'Name', s );
  end

  if DoShowDstImage;
    disp ( '  Show resulting image...' );
    figure;
    imshow ( P );
    s = [ 'Resulting Image ' SrcImageList{CurrentImage} ' after Tone Mapping' ];
    title ( s ); set ( gcf, 'Name', s );
  end
  
    % Time Counting
  IterationTime = cputime - IterationInitTime;
  ElapsedTime = cputime - InitTime;
  ExpectedTime = (ElapsedTime * NumImages) / Iteration;
  MissingTime = (ElapsedTime * (NumImages-Iteration)) / Iteration;
  disp ( sprintf ( '  Last Iteration time %fs Done: %fs/%fs (%fh/%fh)', ...
                   IterationTime, ElapsedTime, ExpectedTime, ElapsedTime / 3600, ExpectedTime / 3600 ) );
  MissingTimeTotH = MissingTime / 3600;
  MissingTimeH = fix ( MissingTimeTotH );
  MissingTimeTotM = ( MissingTimeTotH - MissingTimeH ) * 60.0;
  MissingTimeM = fix ( MissingTimeTotM );
  MissingTimeS = ( MissingTimeTotM - MissingTimeM ) * 60.0;
  disp ( sprintf ( '  Missing time %3.3d:%2.2d:%5.3f', MissingTimeH, MissingTimeM, MissingTimeS ) );
end
