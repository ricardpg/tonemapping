/*
  Autor(s)      : Jordi Ferrer Plana
  e-mail        : jferrerp@eia.udg.edu
  Branch        : Computer Vision

  Working Group : Underwater Vision Lab
  Project       : Correlation Functions
  Homepage      : http://uvl.udg.edu

  Module        : Generic error functions.

  File          : errorMx.cpp
  Date          : 11/10/2007 - 11/10/2007
  Encoding      : ISO-8859-1 (Latin-1)

  Compiler      : MATLAB >= 7.0 & g++ >= 4.x
  Libraries     :

  Notes         :

 -----------------------------------------------------------------------------

  Copyright (C) 2008 by Jordi Ferrer Plana

  This source code is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This source code is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
  more details.

 -----------------------------------------------------------------------------
*/

#include <cstdarg>                     // STL: Variable parameter lists.

#include <mex.h>                       // Matlab: Mex Functions

#include "errorMx.h"                   // Definition

void printfmexErrMsgTxt ( char *Format, ... )
{
  va_list VarPar;                    // Variable parameter list
  char Str[STR_ERROR_BUFFER_SIZE+1]; // Buffer String

  va_start ( VarPar, Format );       // Init Variable parameter list
  vsprintf ( Str, Format, VarPar );  // Print String

  mexErrMsgTxt ( Str );
  
  va_end ( VarPar );                 // Free variable parameter list
}

void printfmexWarnMsgTxt ( char *Format, ... )
{
  va_list VarPar;                    // Variable parameter list
  char Str[STR_ERROR_BUFFER_SIZE+1]; // Buffer String

  va_start ( VarPar, Format );       // Init Variable parameter list
  vsprintf ( Str, Format, VarPar );  // Print String

  mexWarnMsgTxt ( Str );
  
  va_end ( VarPar );                 // Free variable parameter list
}
