%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Tone - Mapping Interface.
%
%  File          : Compile.m
%  Date          : 17/02/2007 - 19/06/2008
%  Encoding      : ISO-8859-1 (Latin-1)
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : 
%
%  Notes         :
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2008 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%

% Detect Matlab Version
MatVer = version ( '-release' );

% 13 for 6.5 or smaller for lower versions and 20 for others (2006a, 2006b, 2007a, 2007b, 2008a)
Major = str2double ( MatVer(1:2) );

% Detect Platform
Arch = computer;

if strcmpi ( Arch, 'pcwin' ); Windows = 1; Nbit = 32;
  elseif strcmpi ( Arch, 'pcwin64' ); Windows = 1; Nbit = 64;
  else Windows = 0;
end

% Where there is the Source ToneMapping Code
ToneMappingSrc = './Base';

% Link Libraries
lLibraries = '';

% Optimization Flags
Flags = '-O';

% IncludePath
IncludePath = [ '-I. ' '-I' ToneMappingSrc ];

% Library path
LibraryPath = '';

FileNames = cell ( 1, 1 );
FileNames{1} = 'tonemappingMx.cpp';
AuxCppFiles = [ 'errorMx.cpp ' ToneMappingSrc '/' 'beolv.cpp ' ToneMappingSrc '/' 'histogram.cpp' ];

% Compile
for i = 1 : size ( FileNames, 1 );
  disp ( sprintf ( 'Compiling "%s"...', FileNames{i} ) );
  Cmd = ['mex ' Flags ' ' IncludePath ' ' LibraryPath ' ' lLibraries ' ' FileNames{i} ' ' AuxCppFiles ];
  disp ( sprintf ( ['\n' Cmd '\n' ] ) );
  eval ( Cmd );
end
