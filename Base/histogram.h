#ifndef __HISTOGRAM_H__
#define __HISTOGRAM_H__

#include <iostream>
#include <cmath>

#include "basegeom.h"
#include "vector.h"
#include "matrix.h"

#ifdef __HISTOGRAM_CPP__ 
#define __HOMEFILE__
#endif
#include "defines.h"

#define minimal_areasize 8 // a menetkozbeni lehetseges minimalis max(sizex,sizey) (ezek lapolnak at egymasra)
#define POSSIBLE_MINIMAL_FELSO_LIMIT 1.005
#define histsize_at_clipping 100000

GLOBAL matrix<double> grayImage;
GLOBAL matrix<vector3<unsigned char> > imagergbbyte;
GLOBAL matrix<vector3<unsigned short> > imagergbshort;
GLOBAL matrix<vector3<unsigned long> > imagergblong;
GLOBAL matrix<vector3<float> > imagergbfloat;
GLOBAL matrix<unsigned char> imagegraybyte;
GLOBAL matrix<unsigned short> imagegrayshort;
GLOBAL matrix<unsigned long> imagegraylong;
GLOBAL matrix<float> imagegrayfloat;
enum imagetype {rgbbyte, rgbshort, rgblong, rgbfloat, rgbnibyte, rgbnishort, rgbnilong, rgbnifloat, graybyte, grayshort, graylong, grayfloat};
GLOBAL imagetype imageType;
GLOBAL bool imageBE;
GLOBAL char INPUT_FILENAME[260], OUTPUT_FILENAME[260];
GLOBAL vector<char> imageHeader;
GLOBAL unsigned int HEADER_LENGTH, IMAGE_SIZE_X, IMAGE_SIZE_Y;
GLOBAL unsigned int ROW_END_DUMMY_BYTES_NUMBER, ROW_END_DUMMY_BYTES_NUMBER_MOD;
GLOBAL double INPUT_BRIGHTEST, INPUT_MEDIA_CONTRAST, OUTPUT_BRIGHTEST, OUTPUT_MEDIA_CONTRAST;
GLOBAL double CONTRAST_CLIPPING_OF_AREA_RATIO_LOWER, CONTRAST_CLIPPING_OF_AREA_RATIO_UPPER;
GLOBAL double MAX_CONTRAST_LIMIT_UPPER_PER_STEP, MIN_CONTRAST_LIMIT_UPPER_PER_STEP;
GLOBAL double input_lower_clipping, input_upper_clipping, log_input_lower_clipping, log_input_upper_clipping;
GLOBAL double output_lower_clipping, output_upper_clipping, log_output_lower_clipping, log_output_upper_clipping;
GLOBAL pureallocator<double> sequenceWeight;
GLOBAL pureallocator<pureallocator<pureallocator<double> > > sequenceUpperLimit;
GLOBAL pureallocator<pureallocator<pureallocator<double> > > sequenceLowerLimit;

inline double _ab_to_cd(double x, double a, double b, double c, double d) { return (x-a)/(b-a)*(d-c)+c; }
inline double _ab_to_01_clip(double x, double a, double b) { double ret=(x-a)/(b-a); return ret<0?0:ret>1?1:ret; }
inline double _01_to_ab(double x, double a, double b) { return (b-a)*x+a; }

inline double file_to_amclahe(double gray) // [a,b] -> [log a,log b] -> [0,1]
{ return _ab_to_01_clip(log(gray), log_input_lower_clipping, log_input_upper_clipping); }

inline double amclahe_to_file(double loggray_01) // [0,1] -> [log a,log b] -> [a,b]
{ return exp(_01_to_ab(loggray_01, log_output_lower_clipping, log_output_upper_clipping)); }

template <class T>
inline double color_to_gray(const vector3<T> & rgbin) // rgbin m�r zajv�gott, alul-fel�l-v�gott, stb
{ return (double)max(max(rgbin.x,rgbin.y),rgbin.z); }

template <class T>
inline vector3<T> gray_to_color(const vector3<T> & rgbin, double grout)  // rgbin m�r zajv�gott, alul-fel�l-v�gott, stb
{ 
double r=color_to_gray(rgbin), c=1; 
	if (r<input_lower_clipping)
	{
		c=input_lower_clipping/r;
		r=input_lower_clipping;
	}
	else if (r>input_upper_clipping)
	{
		c=input_upper_clipping/r;
		r=input_upper_clipping;
	}
	c*=grout/r;
	vector3<T> ret; ret.x=(T)(rgbin.x*c); ret.y=(T)(rgbin.y*c); ret.z=(T)(rgbin.z*c); return ret; 
}

void changeBytes(char * c, int n);

template <class T>
bool readrgb(matrix<vector3<T> > & m, matrix<double> & gray, const char* filename, int width, int height, bool be)
{
int i,j; 
unsigned int ii; 
FILE* file=fopen(filename, "rb"); 
	if (file==NULL)
		return FALSE; 

	m.resize(width, height); 
	gray.resize(width, height); 

	imageHeader.resize(HEADER_LENGTH);
	if (HEADER_LENGTH!=0 && HEADER_LENGTH!=fread(&(imageHeader(0)), 1, HEADER_LENGTH, file))
	{ fclose(file); return FALSE; }

T brightest=(T)INPUT_BRIGHTEST, darkest=(T)(INPUT_BRIGHTEST/INPUT_MEDIA_CONTRAST);
	for (j=0; j<height; j++) 
	{
		for (i=0; i<width; i++) 
		{ 
		vector3<T> & v=m(i,j);
		char *c=(char*)(&v);
			if (3*sizeof(T)!=fread(c, 1, sizeof(T)*3, file))
			{ fclose(file); return FALSE; }
			if (be)
				for (ii=0; ii<3; ii++)
					changeBytes(c+ii*sizeof(T), sizeof(T));
			v.x=max(min(v.x,brightest),darkest); v.y=max(min(v.y,brightest),darkest); v.z=max(min(v.z,brightest),darkest);
			gray(i,j)=color_to_gray(v); 
		}
		char ch;
		for (ii=0; ii<ROW_END_DUMMY_BYTES_NUMBER; ii++)
			if (1!=fread(&ch, 1, 1, file))
			{ fclose(file); return FALSE; }
	}

	fclose(file); 
	return TRUE;
}
template <class T>
bool writergb(const matrix<vector3<T> > & m, const matrix<double> & grayout, const char* filename, bool be)
{
int i,j; 
unsigned int ii; 
FILE* file=fopen(filename, "wb"); 
	if (file==NULL)
		return FALSE; 

	if (HEADER_LENGTH!=0 && HEADER_LENGTH!=fwrite(&(imageHeader(0)), 1, HEADER_LENGTH, file))
	{ fclose(file); return FALSE; }

	for (j=0; j<m.getsizey(); j++) 
	{
		for (i=0; i<m.getsizex(); i++) 
		{ 
		vector3<T> t=gray_to_color(m(i,j), grayout(i,j));
		char *c=(char*)(&t);
			if (be)
				for (ii=0; ii<3; ii++)
					changeBytes(c+ii*sizeof(T), sizeof(T));
			if (3*sizeof(T)!=fwrite(c, 1, sizeof(T)*3, file))
			{ fclose(file); return FALSE; }
		}
		char ch='\0';
		for (ii=0; ii<ROW_END_DUMMY_BYTES_NUMBER; ii++)
			if (1!=fwrite(&ch, 1, 1, file))
			{ fclose(file); return FALSE; }
	}

	fclose(file); 
	return TRUE;
}
template <class T>
bool readrgb_noninterlaced(matrix<vector3<T> > & m, matrix<double> & gray, const char* filename, int width, int height, bool be)
{
int i,j,k; 
unsigned int ii; 
FILE* file=fopen(filename, "rb"); 
	if (file==NULL)
		return FALSE; 

	m.resize(width, height); 
	gray.resize(width, height); 

	imageHeader.resize(HEADER_LENGTH);
	if (HEADER_LENGTH!=0 && HEADER_LENGTH!=fread(&(imageHeader(0)), 1, HEADER_LENGTH, file))
	{ fclose(file); return FALSE; }

T brightest=(T)INPUT_BRIGHTEST, darkest=(T)(INPUT_BRIGHTEST/INPUT_MEDIA_CONTRAST);
	for (k=0; k<3; k++)
	{
		for (j=0; j<height; j++) 
		{
			for (i=0; i<width; i++) 
			{ 
			char *c=(char*)(&(m(i,j)))+k*sizeof(T);
			T *v=(T*)c;
				if (sizeof(T)!=fread(c, 1, sizeof(T), file))
				{ fclose(file); return FALSE; }
				if (be)
					changeBytes(c, sizeof(T));
				*v=max(min(*v,brightest),darkest); 
			}
			char ch;
			for (ii=0; ii<ROW_END_DUMMY_BYTES_NUMBER; ii++)
				if (1!=fread(&ch, 1, 1, file))
				{ fclose(file); return FALSE; }
		}
		for (j=0; j<height; j++) for (i=0; i<width; i++) 
			gray(i,j)=color_to_gray(m(i,j)); 
	}

	fclose(file); 
	return TRUE;
}
template <class T>
bool writergb_noninterlaced(const matrix<vector3<T> > & m, const matrix<double> & grayout, const char* filename, bool be)
{
int i,j,k; 
unsigned int ii; 
FILE* file=fopen(filename, "wb"); 
	if (file==NULL)
		return FALSE; 

	if (HEADER_LENGTH!=0 && HEADER_LENGTH!=fwrite(&(imageHeader(0)), 1, HEADER_LENGTH, file))
	{ fclose(file); return FALSE; }

	for (k=0; k<3; k++)
	for (j=0; j<m.getsizey(); j++) 
	{
		for (i=0; i<m.getsizex(); i++) 
		{ 
		vector3<T> t=gray_to_color(m(i,j), grayout(i,j));
		char *c=(char*)(&t)+k*sizeof(T);
			if (be)
				changeBytes(c, sizeof(T));
			if (sizeof(T)!=fwrite(c, 1, sizeof(T), file))
			{ fclose(file); return FALSE; }
		}
		char ch='\0';
		for (ii=0; ii<ROW_END_DUMMY_BYTES_NUMBER; ii++)
			if (1!=fwrite(&ch, 1, 1, file))
			{ fclose(file); return FALSE; }
	}

	fclose(file); 
	return TRUE;
}
template <class T>
bool readgray(matrix<double> & gray, const char* filename, int width, int height, bool be)
{
int i,j; 
unsigned int ii; 
FILE* file=fopen(filename, "rb"); 
	if (file==NULL)
		return FALSE; 

	gray.resize(width, height); 

	imageHeader.resize(HEADER_LENGTH);
	if (HEADER_LENGTH!=0 && HEADER_LENGTH!=fread(&(imageHeader(0)), 1, HEADER_LENGTH, file))
	{ fclose(file); return FALSE; }

T brightest=(T)INPUT_BRIGHTEST, darkest=(T)(INPUT_BRIGHTEST/INPUT_MEDIA_CONTRAST);
	for (j=0; j<height; j++) 
	{
		for (i=0; i<width; i++) 
		{ 
		T t;
		char *c=(char*)(&t);
			if (sizeof(T)!=fread(c, 1, sizeof(T), file))
			{ fclose(file); return FALSE; }
			if (be)
				changeBytes(c, sizeof(T));
			t=max(min(t,brightest),darkest); 
			gray(i,j)=(double)t; 
		}
		char ch;
		for (ii=0; ii<ROW_END_DUMMY_BYTES_NUMBER; ii++)
			if (1!=fread(&ch, 1, 1, file))
			{ fclose(file); return FALSE; }
	}

	fclose(file); 
	return TRUE;
}
template <class T>
bool writegray(const matrix<double> & gray, const char* filename, bool be)
{
int i,j; 
unsigned int ii; 
FILE* file=fopen(filename, "rb"); 
	if (file==NULL)
		return FALSE; 

	if (HEADER_LENGTH!=0 && HEADER_LENGTH!=fwrite(&(imageHeader(0)), 1, HEADER_LENGTH, file))
	{ fclose(file); return FALSE; }

	for (j=0; j<gray.getsizey(); j++) 
	{
		for (i=0; i<gray.getsizex(); i++) 
		{ 
		T t=(T)gray(i,j);
		char *c=(char*)(&t);
			if (be)
				changeBytes(c, sizeof(T));
			if (sizeof(T)!=fwrite(c, 1, sizeof(T), file))
			{ fclose(file); return FALSE; }
		}
		char ch='\0';
		for (ii=0; ii<ROW_END_DUMMY_BYTES_NUMBER; ii++)
			if (1!=fwrite(&ch, 1, 1, file))
			{ fclose(file); return FALSE; }
	}

	fclose(file); 
	return TRUE;
}
bool readimage();
bool writeimage();

// inline int pround(double x) { return ((int)(x+.5)); } // positive round
// inline int round(double x) { return (x>=0?(int)(x+.5):(int)(x-.5)); } // round

class level0101
{
public:
	vector<double> levels; //!! kumulalt ertek, modositas, visszaszamolas 
public:
	inline level0101(int size=1) : levels(size+1) 
	{ levels[0]=0; }
	inline level0101(const double *tomb,  int size) : levels(size+1) 
	{ for (int i=0; i<=size; i++) levels[i]=tomb[i]; }
	inline level0101(const vector<double> tomb) : levels(tomb) {}
	inline level0101(const level0101 &h) : levels(h.levels) 
	{ levels[0]=0; } 
	inline ~level0101() {} 
	inline double level(double index) const // 01 -> 01
	{ 
	double elem = index*(levels.size-1) - EPSI; 
	int e=(int)elem; 
	double f=elem-e; 
	double ret=levels[e++]*(1-f); 
		if (e<levels.size) 
		ret+=levels[e]*f; 
		return ret; 
	} 
}; 

class histogram : public level0101
{ 
friend inline std::ostream & operator<< (std::ostream & os, const histogram & hist); 
// private: 
public: 
	vector<double> cells; //!! ezt toltjuk a pixelertekek nyoman (put)
private: 
	double ratioofMIN, ratioofMAX; //!! also-folso kontraszt-limit tangensek
public: 
	inline void setmarginsratio(double ratiomax, double ratiomin) 
	{ ratioofMAX=ratiomax, ratioofMIN=ratiomin; }
	inline histogram(int size=1) : level0101(size), cells(size)
	{ 
		for (int i=0; i<size; i++) cells[i]=0; 
		setmarginsratio(BIGNUM, 0.); 
	} 
	inline histogram(const histogram &h) : level0101(h), cells(h.cells) 
	{
		ratioofMIN = h.ratioofMIN; 
		ratioofMAX = h.ratioofMAX; 
	}
	inline ~histogram() {}
	inline void reset(int size, double ratiomax=BIGNUM, double ratiomin=0) 
	{ 
		cells.resize(size); 
		levels.resize(size+1); 
		for (int i=0; i<size; i++) cells[i]=0; 
		setmarginsratio(ratiomax, ratiomin);
	} 
	inline void reset() 
	{ for (int i=0; i<cells.size; i++) { cells[i]=0; } } 
	inline void put(double index) { cells[(int)(index*cells.size - EPSI)]++; }
	inline void put(const matrix<double> & pic) 
	{ 
		for (int i=0; i<pic.getsizex(); i++) for (int j=0; j<pic.getsizey(); j++) 
			put(pic(i,j));
	} 
	inline void put(const histogram & hist) // azonos cells.size eseten !!! 
	{ for (int i=0; i<cells.size; i++) cells[i]+=hist.cells[i]; } 
	void equalize(); //!! ez a lenyeg
}; 

inline std::ostream & operator<< (std::ostream & os, const histogram & hist) 
{
	os << "\nratioofMIN = " << hist.ratioofMIN << "\tratioofMAX = " << hist.ratioofMAX;
	for (int i=0; i<hist.cells.size; i++)
	{ 
		std::cout << "\nindex = " << i 
			<< "\t cells[] = " << hist.cells[i] 
			<< "\ttangent[] = " << (hist.levels[i+1]-hist.levels[i])*hist.cells.size; 
	}
	return (os); 
}

void relativeshift(double & relshift_x, double & relshift_y, int number); 

class histmatr 
{ 
friend inline std::ostream & operator << (std::ostream & os, const histmatr & mhist); 
	matrix<histogram> mhist; 
	int cornerx, cornery, picsizex, picsizey; 
	int areasize; 
public: 
	inline ~histmatr() {} 
	inline histmatr() {} 
	void reset(int picsizex_, int picsizey_, int histsize_, double histlimit_felso, double histlimit_also, int terfrek_ism_, int areasize_); 
	inline histmatr(int picsizex_, int picsizey_, int histsize_, double histlimit_felso, double histlimit_also, int terfrek_ism_, int areasize_)
	{ reset(picsizex, picsizey, histsize_, histlimit_felso, histlimit_also, terfrek_ism_, areasize_); }
	inline histmatr(const matrix<double> & pic, int histsize_, double histlimit_felso, double histlimit_also, int terfrek_ism_, int areasize_)
	{ reset(pic.getsizex(), pic.getsizey(), histsize_, histlimit_felso, histlimit_also, terfrek_ism_, areasize_); put(pic); } 
	inline void put(double index, int x, int y) 
	{ mhist((x-cornerx)/areasize,(y-cornery)/areasize).put(index); } 
	inline void put(const matrix<double> & pic) 
	{ 
		for (int i=0; i<pic.getsizex(); i++) for (int j=0; j<pic.getsizey(); j++) 
			put(pic(i,j), i, j);
	} 
	inline void extend() // eloszor kisnegyzetekbe kerul, aztan az atlapolashoz masik harmat hozzaad
	{ 
		for (int i=mhist.getsizex()-1; i>=0; i--) 
		for (int j=mhist.getsizey()-1; j>=0; j--) 
		{ 
			if (i-1>=0) mhist(i,j).put(mhist(i-1,j)); 
			if (j-1>=0) mhist(i,j).put(mhist(i,j-1)); 
			if (i-1>=0&&j-1>=0) mhist(i,j).put(mhist(i-1,j-1)); 
		} 
	} 
	inline void equalize() 
	{ 
		for (int i=0; i<mhist.getsizex(); i++) for (int j=0; j<mhist.getsizey(); j++) 
			mhist(i,j).equalize();
	}
	inline double level(double index, int x, int y) const//level
	{ return (mhist((x-cornerx)/areasize,(y-cornery)/areasize).level(index)); } 
	inline double elevel(double index, int x, int y) const//elevel 
	{ 
		x-=cornerx; y-=cornery; 
	double wx, wy, ret=0; // , w=0; 
	int xx, yy, i,j; 
		for (i=0, xx=x/areasize, wx=1.-(0.5+x-(double)xx*areasize)/areasize; i<2; i++, wx=1.-wx, xx++) 
		for (j=0, yy=y/areasize, wy=1.-(0.5+y-(double)yy*areasize)/areasize; j<2; j++, wy=1.-wy, yy++) 
		{ ret+=wx*wy*mhist(xx,yy).level(index); }
//			w+=wx*wy; // debug 
		return (ret); 
	} 
	inline void traf(matrix<double> & picout, const matrix<double> & picin) 
	{ 
		picout.resize(picin.getsizex(), picin.getsizey()); 
		for (int i=0; i<picin.getsizex(); i++) for (int j=0; j<picin.getsizey(); j++) 
		{ picout(i,j)=(float)(elevel(picin(i,j),i,j)); } 
	} 
}; 
inline std::ostream & operator << (std::ostream & os, const histmatr & hm)
{
	os << "\nareasize = " << hm.areasize;
	os << "\npicsize = " << hm.picsizex << "," << hm.picsizey; 
	os << "\tcorner = " << hm.cornerx << "," << hm.cornery; 
	os << "\n\n mhist : ";
	for (int i=0; i<hm.mhist.getsizex(); i++) 
	for (int j=0; j<hm.mhist.getsizey(); j++) 
	{ 
		std::cout << "\n\nindex = " << i << " , " << j << "\n"; 
		std::cout << hm.mhist(i,j); 
	}
	return (os); 
}

////////////////////////////////////////////////////////////// 

bool inputFromParfile(const char *parfilename);
void contrastClipping();
bool workFromParfile(const char *parfilename);

////////////////////////////////////////////////////////////// 

#include "undefs.h"

#endif // __HISTOGRAM_H__
