// MANUAL matrix template and its auxiliary classes

#ifndef __MATRIX_H__
#define __MATRIX_H__

#include <iostream>

#include "basegeom.h" 

// MANUAL closed interval with union, intersection, distance, etc - see code
class interval { // closed interval
public:
	int m, M;
	inline interval() { m=M=0; }
	inline interval(int _m) { m=_m; M=_m; }
	inline interval(int _m, int _M) { m=_m; M=_M; }
	inline interval(const interval & q) { m=q.m; M=q.M; }
	inline ~interval() {}
	inline bool valid() const { return m<=M; }
	inline int size() const { return M-m+1; }
// MANUAL union
	inline interval & operator|=(const interval & q) { m=min(m,q.m); M=max(M,q.M); return *this;	}
	inline interval operator|(const interval & q) const { return interval(min(m,q.m), max(M,q.M)); }
	inline interval & operator|=(int q) { m=min(m,q); M=max(M,q); return *this;	}
	inline interval operator|(int q) const { return interval(min(m,q), max(M,q)); }
// MANUAL intersection
	inline interval & operator&=(const interval & q) { m=max(m,q.m); M=min(M,q.M); return *this; }
	inline interval operator&(const interval & q) const { return interval(max(m,q.m), min(M,q.M)); }
// MANUAL distance
	inline int dist(const int q) const 
	{ 
		if (m>=q) return m-q; 
		else if (q>=M) return q-M; 
		else return 0;
	}
	inline int dist(const interval & q) const 
	{ 
		if (m>=q.M) return m-q.M; 
		else if (q.m>=M) return q.m-M; 
		else return 0;
	}
// MANUAL shift
	inline interval & operator<<=(int i) { m-=i; return *this; }
	inline interval operator<<(int i) { return interval(m-i,M);	}
	inline interval & operator>>=(int i) { M+=i; return *this; }
	inline interval operator>>(int i) { return interval(m,M+i);	}
};

inline std::ostream & operator<< (std::ostream & os, const interval & i)
{ return os << "( m : " << i.m << " ,  M : " << i.M << " ) "; }
inline std::istream & operator>> (std::istream & is, interval & i)
{ return is >> iodummy >> iodummy >> iodummy >> i.m 
		>> iodummy >> iodummy >> iodummy >> i.M >> iodummy; }

// MANUAL quad with the same logic as of interval
class quad {
public:
	interval x, y;
	inline quad() { }
	inline quad(const interval & _x, const interval & _y) : x(_x), y(_y) { }
	inline quad(int xmin, int ymin) : x(xmin), y(ymin) { }
	inline quad(int xmin, int ymin, int xmax, int ymax) : x(xmin,xmax), y(ymin,ymax) { }
	inline quad(const quad & q) : x(q.x), y(q.y) { }
	inline ~quad() {}
	inline bool valid() const { return x.valid() && y.valid(); }
	inline vector2<int> size() const { return vector2<int>(x.size(),y.size()); }
// MANUAL union
	inline quad & operator|=(const quad & q) { x|=q.x; y|=q.y; return *this; }
	inline quad operator|(const quad & q) const { return quad(x|q.x, y|q.y); }
	inline quad & operator|=(const vector2<int> & q) { x|=q.x; y|=q.y; return *this; }
	inline quad operator|(const vector2<int> & q) const { return quad(x|q.x, y|q.y); }
// MANUAL intersection
	inline quad & operator&=(const quad & q) { x&=q.x; y&=q.y; return *this; }
	inline quad operator&(const quad & q) const	{ return quad(x&q.x, y&q.y); }
// MANUAL distance (2d)
	inline int dist(const int _x, const int _y) const { return max(x.dist(_x), y.dist(_y)); }
	inline int dist(const vector2<int> & q) const { return dist(q.x, q.y); }
	inline int dist(const quad & q) const { return max(x.dist(q.x), y.dist(q.y)); }
// MANUAL shift
	inline quad & operator<<=(vector2<int> ii) { x<<=ii.x; y<<=ii.y; return *this; }
	inline quad operator<<(vector2<int> ii) { return quad(x<<ii.x,y<<ii.y); }
	inline quad & operator>>=(vector2<int> ii) { x>>=ii.x; y>>=ii.y; return *this; }
	inline quad operator>>(vector2<int> ii) { return quad(x>>ii.x,y>>ii.y); }
};

inline std::ostream & operator<< (std::ostream & os, const quad & q)
{ return os << "( x : " << q.x << " ,  y : " << q.y << " ) "; }
inline std::istream & operator>> (std::istream & is, quad & q)
{ return is >> iodummy >> iodummy >> iodummy >>
		q.x >> iodummy >> iodummy >> iodummy >> q.y >> iodummy; 
}

// MANUAL matrix template
template <class T>
class matrix 
{
public:
	T *elem;
protected:
	int sizex, sizey; // x row, y column
public:
	inline matrix(int _sizex, int _sizey) : sizex(_sizex), sizey(_sizey) { elem = new T[sizex*sizey]; }
	inline matrix(int _sizex, int _sizey, const T & initial) : sizex(_sizex), sizey(_sizey) 
	{ 
		elem = new T[sizex*sizey];
		this->operator =(initial);
	}
	inline matrix() { sizex=0; sizey=0; elem = new T[0]; }
	inline matrix(const matrix<T> &p) 
	{
		sizex = p.getsizex();
		sizey = p.getsizey();
		elem = new T[sizex*sizey];
		for (int i=0; i<sizex; i++) 
		{
			for (int j=0; j<sizey; j++) 
			{
					//T temp = p(i, j);
					(*this)(i, j) = p(i, j); //=temp;
			}
		}
	};
	inline matrix & operator =(const matrix<T> & p) 
	{ 
		if (this != (matrix<T>*)(&p)) 
		{
			resize(p.getsizex(), p.getsizey());
			for (int i=0; i<sizex; i++) 
			{
				for (int j=0; j<sizey; j++) 
				{
					//T temp = p(i, j);
					(*this)(i, j) = (T)p(i, j); //=temp;
				}
			}
		}
		return (*this);
	}
	inline matrix & operator =(const T & initial) 
	{
		for (int i=0; i<sizex; i++) 
		{
			for (int j=0; j<sizey; j++) 
			{
				(*this)(i, j) = initial;
			}
		}
		return (*this);
	}
	inline void swap(matrix<T> & p) 
	{
		int tempx = p.getsizex(), tempy = p.getsizey();
		p.sizex = sizex; sizex = tempx;
		p.sizey = sizey; sizey = tempy;
		T *tempelem = p.elem;
		p.elem = elem; 
		elem = tempelem;
	}
	inline ~matrix() { if (elem) { delete [] elem; elem = NULL; } }
	inline int getsizex() const { return sizex; }  
	inline int getsizey() const { return sizey; }  
	inline void setsizex(const int newsizex) 
	{ 
		if (sizex == newsizex) return;
		T *newelem = new T[newsizex*sizey];
		int i, j, to = min(sizex, newsizex);
		for (i=0; i<to; i++) 
		{
			for (j=0; j<sizey; j++) 
			{
				newelem[i*sizey+j] = (*this)(i,j);
			}
		}
		if ( elem ) { delete [] elem; elem = NULL; }
		elem = newelem;
		sizex = newsizex;
	}
	inline void setsizey(const int newsizey) // szelesseg
	{ 
		if (sizey == newsizey) return;
		T *newelem = new T[sizex*newsizey];
		int i, j, to = min(sizey, newsizey);
		for (i=0; i<sizex; i++) 
		{
			for (j=0; j<to; j++) 
			{
				newelem[i*sizey+j] = (*this)(i,j);
			}
		}
		if ( elem ) { delete [] elem; elem = NULL; }
		elem = newelem;
		sizey = newsizey;
	}
// MANUAL reallocate the whole matrix
	inline void resize(const int newsizex, const int newsizey) 
	{
		if ((sizex==newsizex) && (sizey==newsizey)) return;
		T *newelem = new T[newsizex*newsizey];
		int i, toi = min(sizex, newsizex), j, toj = min(sizey, newsizey);
		for (i=0; i<toi; i++) 
		{
			for (j=0; j<toj; j++) 
			{
				newelem[i*newsizey+j] = (*this)(i, j);
			}
		}
		if (elem) { delete [] elem; elem = NULL; }
		elem = newelem;
		sizex = newsizex;
		sizey = newsizey;
	}
	inline const T & operator ()(const int i, const int j) const // i. row, j. column
	{ 
#ifdef _DEBUG
		if ((i>=sizex) || (j>=sizey)) 
			throw "overindexed matrix";
#endif
		return elem[i*sizey+j];
	}
	inline T & operator ()(const int i, const int j) // i. sor, j. oszlop
	{
		#ifdef _DEBUG
			if ((i>=sizex) || (j>=sizey)) 
				throw "overindexed matrix";
		#endif
		return elem[i*sizey+j];
	}
// MANUAL routines for Summmed Area Table
//	template <class TT>
	inline matrix & setSAT(const matrix<T>//<TT> 
			& m) 
	{ 
		resize(m.sizex+1, m.sizey+1);
	int i,j;
		for (i=0; i<sizex; i++) 
			operator()(i,0)=(T)0;
		for (j=0; j<sizey; j++) 
			operator()(0,j)=(T)0; 
		for (i=1; i<sizex; i++) 
		for (j=1; j<sizey; j++) 
		{ 
			operator()(i,j) = operator()(i,j-1) + operator()(i-1,j) +//(TT)
				m(i-1,j-1);
			operator()(i,j) -= operator()(i-1,j-1); // ...
		}
		return *this;
	}
	inline int openrectofSAT(T & ret, int xm, int ym, int xp, int yp) const // down closed, up open interval
	{ 
		ret = operator()(xp,yp)+operator()(xm,ym); 
		ret-=operator()(xp,ym); // ...
		ret-=operator()(xm,yp); // ...
		return (xp-xm)*(yp-ym);
	}
	inline T openrectofSAT(int xm, int ym, int xp, int yp) const // down closed, up open interval
	{ 
	T ret = operator()(xp,yp)+operator()(xm,ym); 
		ret-=operator()(xp,ym); // ...
		ret-=operator()(xm,yp); // ...
		return ret;
	}
	inline T closedrectofSAT(int xm, int ym, int xp, int yp) const 
	{ 
		return openrectofSAT(xm, ym, xp+1, yp+1);
	}
	inline int closedrectofSAT(T & ret, int xm, int ym, int xp, int yp) const 
	{ 
		return openrectofSAT(ret, xm, ym, xp+1, yp+1);
	}
	inline T elemfromSAT(int x, int y) const 
	{
		return openrectofSAT(x, y, x+1, y+1);
	}
	inline int elemfromSAT(T & ret, int x, int y) const 
	{
		return openrectofSAT(ret, x, y, x+1, y+1);
	}
	inline T openrectofSAT(quad q) const // down closed, up open interval
	{
		return openrectofSAT(q.x.m, q.y.m, q.x.M, q.y.M);
	}
	inline int openrectofSAT(T & ret, quad q) const // down closed, up open interval
	{ 
		return openrectofSAT(ret, q.x.m, q.y.m, q.x.M, q.y.M);
	}
	inline T closedrectofSAT(quad q) const 
	{ 
		return closedrectofSAT(q.x.m, q.y.m, q.x.M, q.y.M);
	}
	inline int closedrectofSAT(T & ret, quad q) const 
	{ 
		return closedrectofSAT(ret, q.x.m, q.y.m, q.x.M, q.y.M);
	}
	inline matrix<T> & operator +=(const matrix<T> &p) 
	{ for(int i=0; i<sizex; i++) for(int j=0; j<sizey; j++) operator()(i,j)+=p(i,j); return (*this); }
	inline matrix<T> & operator -=(const matrix<T> &p) 
	{ for(int i=0; i<sizex; i++) for(int j=0; j<sizey; j++) operator()(i,j)-=p(i,j); return (*this); }
	inline matrix<T> & operator ^=(const matrix<T> &p) 
	{ for(int i=0; i<sizex; i++) for(int j=0; j<sizey; j++) operator()(i,j)*=p(i,j); return (*this); }
	inline matrix<T> & operator *=(const T & c) 
	{ for(int i=0; i<sizex; i++) for(int j=0; j<sizey; j++) operator()(i,j)*=c; return (*this); }
	inline matrix<T> & operator /=(const T & c) 
	{ for(int i=0; i<sizex; i++) for(int j=0; j<sizey; j++) operator()(i,j)/=c; return (*this); }
};

template <class T>
inline std::ostream & operator<<(std::ostream &os, const matrix<T> &mtx) 
{
//	indent;
	os << std::endl << "( size : ( " << mtx.getsizex() << " , " << mtx.getsizey() 
		<< " ) , elem : ";
	{
		for (int i=0;i<mtx.getsizex();i++) 
		{
		////indent;	
		os << "  ";
		os << std::endl << "( " << i << " : ";
			for (int j=0;j<mtx.getsizey();j++)
				os << mtx(i, j) << " , ";
			os << ") ";
		}
	}
	return os << ") ";
}
template <class T>
inline std::istream & operator>>(std::istream &is, matrix<T> &mtx) 
{
int sizex, sizey, ind;
	is >> iodummy >> iodummy >> iodummy >> iodummy 
		>> sizex >> iodummy >> sizey >> iodummy >> iodummy >> iodummy >> iodummy;
	mtx.resize(sizex, sizey);
	for (int i=0; i<mtx.getsizex(); i++) 
	{
		is >> iodummy >> ind >> iodummy;
		for (int j=0;j<mtx.getsizey();j++) 
			is >> mtx(i, j) >> iodummy;
		is >> iodummy;
	}
	return is >> iodummy;
}

#endif // __MATRIX_H__
