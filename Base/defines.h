// DEFINES.H

#include "undefs.h"

#ifdef __HOMEFILE__ 
  #define GLOBAL
  #define STATIC static 
  #define EQUALS =
  #define DEFINITION(code) code 
  #define STATIC_MEMBER

#else

  #define GLOBAL extern
  #define STATIC /##/ 
  #define EQUALS ;/##/
  #define DEFINITION(code) 
  #define STATIC_MEMBER /##/
#endif

#undef __HOMEFILE__ 
