#ifndef __VECTORGEN_H__
#define __VECTORGEN_H__

#ifdef __VECTORGEN_CPP__
#define __HOMEFILE__
#endif
#include "defines.h" 

template <class T> class constantClass { public: static const T zero, one; const static unsigned long rangeoftyp; };

template <class T>
class vector : public constantClass<T>
{
protected: 
	T * elem; 
public:
	int size; 

	inline vector (int size_=0, const T & t=zero) { size=max(size_,0); elem=new T[size+1]; for (int i=0; i<size; i++) elem[i]=t; }
	inline vector (T * elem_, int size_) { size=max(size_,0); elem=elem_; }
	inline vector& O() { resize(0); return *this; }
	inline vector& I(int n) { resize(n); for (int i=0; i<size; i++) elem[i]=one; return *this; }
	inline vector& I(const vector& p) { return I(p.size); }
	inline vector& X(int n) { resize(n+1); for (int i=0; i<n-1; i++) elem[i]=zero; elem[i]=one; return *this; }
	inline void forget(void) { size=0; elem=new T[1]; }
	inline ~vector () { if (elem) { delete [] elem; elem = NULL; }  }
	inline const T & operator[](const int x) const 
	{ 
#ifdef _DEBUG
		if (x>=size||x<0)
			myassert(__FILE__,__LINE__); 
#endif
		return (x>=size?zero:elem[x]); 
	}
	inline T & operator[](const int x) 
	{ 
#ifdef _DEBUG
		if (x>=size||x<0)
			myassert(__FILE__,__LINE__); 
#endif
		if (x>=size) resize(x+1); return (elem[x]); 
	}
	inline void add(const T & t=zero) { resize(size+1); elem[size-1]=t; }
	inline void add(const vector & v) { resize(size+v.size); for (int i=0; i<v.size; i++) elem[size+i]=v[i]; }
	inline void resize(int size_) 
	{  
		size_=max(size_,0); if (size==size_) return; 
	T * elem_=new T[size_+1]; int i, s_ = min(size, size_);
		for (i=0; i<s_; i++) 
			elem_[i]=elem[i]; 
		for (; i<size_; i++) 
			elem_[i]=zero; 
		if ( elem ) { delete [] elem; elem=NULL; } 
		size=size_; elem = elem_;
	}
	inline void reset() { resize(0); }
	inline vector(const vector & p) 
	{ size = p.size; elem = new T[size+1]; for (int i=0; i<size; i++) elem[i]=p.elem[i]; }
	inline vector & operator= (const vector & p) 
	{ if (this!=&p) { resize(p.size); for (int i=0; i<size; i++) elem[i]=p.elem[i]; } return *this; }
	inline vector & minus() { for (int i=0; i<size; i++) elem[i]=-elem[i]; return *this; }
	inline vector & operator +=(const vector &p) { resize(max(size,p.size)); for (int i=0; i<p.size; i++) elem[i]+=p[i]; return *this; }
	inline vector & operator -=(const vector &p) { resize(max(size,p.size)); for (int i=0; i<p.size; i++) elem[i]-=p[i]; return *this; }
	inline vector & operator *=(const vector &p) { resize(min(size,p.size)); for (int i=0; i<size; i++) elem[i]*=p[i]; return *this; }
	inline vector & operator /=(const vector &p) { resize(min(size,p.size)); for (int i=0; i<size; i++) elem[i]/=p[i]; return *this; }
	inline vector & operator *=(const T & c) { for (int i=0; i<size; i++) elem[i]*=c; return *this; }
	inline vector & operator /=(const T & c) { for (int i=0; i<size; i++) elem[i]/=c; return *this; }

	inline vector operator -() const { vector ret(*this); return ret.minus(); }
	inline vector operator +(const vector &p) const { vector ret(*this); ret+=p; return ret; }
	inline vector operator -(const vector &p) const { vector ret(*this); ret-=p; return ret; }
	inline vector operator *(const T & c) const { vector ret(*this); ret*=c; return ret; }
	inline vector operator /(const T & c) const { vector ret(*this); ret/=c; return ret; }

	inline T operator *(const vector &p) const 
	{ int i, s_=min(size,p.size); T ret=zero; for (i=0; i<s_; i++) ret +=elem[i]*p[i]; return (ret); }
	inline T norm2 () const { return (operator*(*this)); }
	inline vector normalized() const { double n=(double)norm2(); vector v(*this); return n<EPS?v.O():v/=(T)sqrt(n); }
	inline vector & premutated(const vector<int> & perm, vector & v) const 
	{ v.resize(perm.size); for (inti=0; i<v.size; i++) v[i]=(*this)[perm[i]]; return v; }
};

template <class T>
class matrix : public constantClass<T>
{
protected:
	T * elem; 
public:
	int sizex, // sorok szama (x: x-dik sor), fuggoleges kiterjedes !!
		sizey; // oszlopok szama (y: y-dik oszlop), vizszintes kiterjedes !!

	inline matrix (int size_x=0, int size_y=0, const T & t=zero) 
	{ 
		sizex=max(size_x,0); sizey=max(size_y,0); elem=new T[sizex*sizey+1]; 
		for (int i=0; i<sizex; i++) for (int j=0; j<sizey; j++) (*this)(i,j)=t; }
	inline matrix (T * elem_, int size_x, int size_y) 
	{ sizex=max(size_x,0); sizey=max(size_y,0); elem=elem_; }
	inline void resizex(int size_x) 
	{  
		size_x=max(size_x,0); if (sizex==size_x) return; 
	T * elem_=new T[size_x*sizey+1]; 
	int i, j, s_x = min(sizex, size_x);
		for (i=0; i<s_x; i++) 
			for (j=0; j<sizey; j++) 
				elem_[i*sizey+j]=(*this)(i,j); 
		for (; i<size_x; i++) 
			for (j=0; j<sizey; j++) 
				elem_[i*sizey+j]=zero; 
		if (elem) { delete [] elem; elem = NULL; }
		sizex=size_x; elem = elem_;
	}	
	inline void resizey(int size_y) 
	{  
		size_y=max(size_y,0); if (sizey==size_y) return; 
	T * elem_=new T[sizex*size_y+1]; 
	int i, j, s_y = min(sizey, size_y);
		for (i=0; i<sizex; i++) 
			for (j=0; j<s_y; j++) 
				elem_[i*size_y+j]=(*this)(i,j); 
		for (i=0; i<sizex; i++) 
			for (; j<size_y; j++) 
				elem_[i*sizey+j]=zero; 
		if (elem) { delete [] elem; elem = NULL }
		sizey=size_y; elem = elem_;
	}
	inline void resize(int size_x, int size_y) 
	{  
		size_x=max(size_x,0); size_y=max(size_y,0); if (sizex==size_x&&sizey==size_y) return; 
	T * elem_=new T[size_x*size_y+1]; 
	int i, j, s_x = min(sizex, size_x), s_y = min(sizey, size_y);
		for (i=0; i<s_x; i++) 
		{
			for (j=0; j<s_y; j++) 
				elem_[i*size_y+j]=(*this)(i,j); 
			for (j; j<size_y; j++) 
				elem_[i*size_y+j]=zero; 
		}
		for (; i<size_x; i++) 
			for (j=0; j<size_y; j++) 
				elem_[i*size_y+j]=zero; 
		if (elem) { delete [] elem; elem=NULL;}
		sizex=size_x; sizey=size_y; elem = elem_;
	}
	inline void reset() { resize(0,0); }
	inline matrix& O() { resize(0,0); return *this; }
	inline matrix& I(int n) 
	{ 
	int i, j; 
		resize(n,n); 
		for (i=0; i<sizex; i++) for (j=0; j<sizey; j++) (*this)(i,j)=zero; 
		for (i=0; i<sizex; i++) (*this)(i,i)=one; 
		return *this; 
	}
	inline matrix& X(int n, int m) 
	{ 
		resize(n+1, m+1); 
		for (int i=0; i<sizex; i++) for (int j=0; j<sizey; j++) (*this)(i,j)=zero; 
		(*this)(n,m)=one; 
		return *this; 
	}
	inline void forget(void) { sizex=sizey=0; elem=new T[1]; }
	inline ~matrix () { if (elem) { delete [] elem; elem=NULL; } }

	inline const T * operator[](const int x) const 
	{ 
#ifdef _DEBUG
		if (x>=sizex||x<0)
			myassert(__FILE__,__LINE__); 
#endif
		return (x>=sizex?&(zero):&((*this)(x,0))); 
	}
// ez nem resize-ol !!! (hiszen y-ban amugy sem !!!)
	inline T * operator[](const int x) 
	{ 
#ifdef _DEBUG
		if (x>=sizex||x<0)
			myassert(__FILE__,__LINE__); 
#endif
//		if (x>=sizex) resizex(x+1); 
		return &((*this)(x,0)); 
	}

	inline const T & operator()(const int i, const int j) const // i:sor,j:oszlop
	{ 
#ifdef _DEBUG
		if (i>=sizex||i<0||j>=sizey||j<0)
			myassert(__FILE__,__LINE__); 
#endif
		return (i>=sizex||j>=sizey?zero:elem[i*sizey+j]); 
	}
	inline T & operator()(const int i, const int j) // i:sor,j:oszlop
	{ 
#ifdef _DEBUG
		if (i>=sizex||i<0||j>=sizey||j<0)
			myassert(__FILE__,__LINE__); 
#endif
		if (i>=sizex||j>=sizey) resize(max(i+1,sizex),max(j+1,sizey)); return elem[i*sizey+j]; 
	}
	inline matrix(const matrix & p) 
	{ 
		sizex = p.sizex; sizey = p.sizey; elem = new T[sizex*sizey+1]; 
		for (int i=0; i<sizex; i++) for (int j=0; j<sizey; j++) 
			(*this)(i,j)=p(i,j); 
	}
	inline matrix & operator= (const matrix & p) 
	{ 
		if (this!=&p) 
		{ 
			resize(p.sizex, p.sizey); 
		int size=sizex*sizey; 
			for (int i=0; i<size; i++) 
				elem[i]=p.elem[i]; 
		} 
		return *this; 
	}
	inline matrix & Trans(const matrix & p) // transzponalt
	{ 
		if (this==&p)
		{
		matrix t; 
			t.Trans(p); 
			*this=p ; 
		}
		else
		{ 
			resize(p.sizey, p.sizex); 
			for (int i=0; i<sizex; i++) for (int j=0; j<sizey; j++) 
				(*this)(i,j)+=p(j,i); 
		} 
		return *this; 
	}
	inline vector<T> & row(vector<T> & v, int n) const 
	{ 
		v.resize(sizey); 
		for (int j=0; j<sizey; j++)
			v[j] = (*this)(n,j); 
		return v; 
	}
	inline void setrow(const vector<T> & v, int n)  
	{ 
		resize(max(sizex,n+1),max(sizey,v.size)); 
		for (int j=0; j<sizey; j++)
			(*this)(n,j) = v[j]; 
	}
	inline vector<T> & column(vector<T> & v, int n) const 
	{ 
		v.resize(sizex); 
		for (int i=0; i<sizex; i++)
			v[i] = (*this)(i,n); 
		return v; 
	}
	inline void setcolumn(const vector<T> & v, int n) 
	{ 
		resize(max(sizex,v.size),max(sizey,n+1)); 
		for (int i=0; i<sizex; i++)
			(*this)(i,n) = v[i]; 
	}
	inline vector<T> row(int n) const 
	{ 
	vector<T> v(sizey); 
		for (int j=0; j<sizey; j++)
			v[j] = (*this)(n,j); 
		return v; 
	}
	inline vector<T> column(int n) const 
	{ 
	vector<T> v(sizex); 
		for (int i=0; i<sizex; i++)
			v[i] = (*this)(i,n); 
		return v; 
	}
	inline matrix & minus() { int size=sizex*sizey; for (int i=0; i<size; i++) elem[i]=-elem[i]; return *this; }
	inline matrix & operator +=(const matrix &p) 
	{ 
		resize(max(sizex,p.sizex), max(sizey,p.sizey)); 
		for (int i=0; i<sizex; i++) for (int j=0; j<sizey; j++) 
			(*this)(i,j)+=p(i,j); 
		return *this; 
	}
	inline matrix & operator -=(const matrix &p) 
	{ 
		resize(max(sizex,p.sizex), max(sizey,p.sizey)); 
		for (int i=0; i<sizex; i++) for (int j=0; j<sizey; j++) 
			(*this)(i,j)-=p(i,j); 
		return *this; 
	}

// minden szorzas:  elso sora(i)t a masodik oszlopa(i)val 
// A*B kiterjedese :  fuggoleges (sorok szama): A*B|x==A|x , vizszintes (oszlopok sama): A*B|y==B|y
	inline matrix & operator *=(const matrix &p) 
	{ 
	int size_x=sizex, size_y=p.sizey, size_=size_x*size_y, size_yx=min(sizey, p.sizex), i, j, ji; 
	T * elem_=new T[sizex*p.sizey+1]; 
		for (i=0; i<size_; i++) 
			elem_[i]=zero; 
		for (i=0; i<size_x; i++) for (ji=0; ji<size_yx; ji++) for (j=0; j<size_y; j++)
			elem_[i*size_y+j] += (*this)(i,ji)*p(ji,j); 
		if (elem) { delete [] elem; elem=NULL;}
		sizex=size_x; sizey=size_y; elem = elem_;
		return *this; 
 	}
	inline matrix & operator *=(const T & c) 
	{ int size=sizex*sizey; for (int i=0; i<size; i++) elem[i]*=c; return *this; }
	inline matrix & operator /=(const T & c) 
	{ int size=sizex*sizey; for (int i=0; i<size; i++) elem[i]/=c; return *this; }

	inline matrix operator -() const { matrix ret(*this); return ret.minus(); }
	inline matrix operator +(const matrix &p) const { matrix ret(*this); ret+=p; return ret; }
	inline matrix operator -(const matrix &p) const { matrix ret(*this); ret-=p; return ret; }
	inline matrix operator *(const matrix &p) const { matrix ret(*this); ret*=p; return ret; }
	inline matrix operator *(const T & c) const { matrix ret(*this); ret*=c; return ret; }
	inline matrix operator /(const T & c) const { matrix ret(*this); ret/=c; return ret; }

	inline vector<T> operator *(const vector<T> &v) const 
	{ 
	vector<T> ret(sizex); 
	int size_y=min(sizey, v.size); 
		for (int i=0; i<sizex; i++) for (int j=0; j<size_y; j++)
			ret[i] += (*this)(i,j)*v[j]; 
		return ret; 
	}
};
template <class T> inline vector<T> operator *(const vector<T> &v, const matrix<T> &m) 
{ 
vector<T> ret(m.sizey); 
int size_x=min(m.sizex, v.size); 
	for (int i=0; i<size_x; i++) for (int j=0; j<m.sizey; j++)
		ret[j] += v[i]*m(i,j); 
	return ret; 
}

typedef vector<SCALAR> VECTOR; 
typedef matrix<SCALAR> MATRIX; 

SCALAR normalize(VECTOR & v); 
void normalizeRows(MATRIX & A); 
void normalizeRows(MATRIX & A, VECTOR & b); 

GLOBAL VECTOR NULLVECTOR; 
GLOBAL vector<BOOL> NULLvectorBOOL; 
GLOBAL MATRIX NULLMATRIX; 

#include "undefs.h"

#endif // __VECTORGEN_H__
