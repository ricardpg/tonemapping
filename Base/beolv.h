#ifndef __BEOLV_H__ 
#define __BEOLV_H__ 

#include "basics.h"
#include "vector.h"

//#ifdef __BEOLV_CPP__ 
  #define __HOMEFILE__
//#endif

#include "defines.h"

char* toupper(char* ret, const char* str);
const char* toupper(const char* str);
char* tolower(char* ret, const char* str);
const char* tolower(const char* str);

class textinput
{
private:
	FILE * file;
	bool upper;
	namestring fname;
	pureallocator<char> line;
	pureallocator<char*> tag;
	int currentTag;
	static char commentCharacter;
	void reset() { line.resize(1,'\0'); tag.resize(1,&(line(0))); currentTag=0; }
public:
	textinput() : line(1,'\0'), tag(1,&(line(0))) { file=0; currentTag=0; upper=true; }
	textinput(const char* filename, bool _upper=true) : line(1,'\0'), tag(1,&(line(0))) 
	{ strcpy(fname, filename); file=fopen(filename, "r"); currentTag=0; upper=_upper; }
	bool isOpen() { return file!=0; }
	bool open(const char* filename, bool _upper=true) 
	{ close(); strcpy(fname, filename); file=fopen(filename, "r"); upper=_upper; return isOpen(); }
	void close() { reset(); if (isOpen()) fclose(file); }
	~textinput() { close(); }
	bool getNextLine(); // closes file if false 
	bool getFirstLine() { return open(fname) && getNextLine(); }
	bool getLine(const char* keyword);
	bool lineIsValid() { return line(0)!='\0'; }
	char* getTag() { return tag(currentTag); }
	char* getFirstTag() { currentTag=0; return getTag(); }
//	char* getTitle() { return tag(0); }
	bool tagIsValid() { return tag.getsize()-1>currentTag; }
	char* getNextTag() { if (tagIsValid()) currentTag++; return getTag(); }
	void toupper() { for (int i=0; i<tag.getsize(); i++) toupper(i); }
	void toupper(int i) { if (i<tag.getsize()) for (char* c=tag(i); *c!='\0'; c++) *c=(char)::toupper(*c); }
};

//STATIC_MEMBER char textinput::commentCharacter EQUALS '#';

////////////////////////////////////////////////////////////// 

//#include "undefs.h"

#endif // __BEOLV_H__ 
