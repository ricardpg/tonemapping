#define __HISTOGRAM_CPP__

#include "histogram.h"
#include "beolv.h"

void changeBytes(char * c, int n)
{
	for (int i=0; i<n/2; i++)
	{ char cc=*(c+i); *(c+i)=*(c+n-i-1); *(c+n-i-1)=cc; }
}
bool readimage()
{
	switch (imageType)
	{
	case rgbbyte: return readrgb(imagergbbyte, grayImage, INPUT_FILENAME, IMAGE_SIZE_Y, IMAGE_SIZE_X, imageBE);
	case rgbshort: return readrgb(imagergbshort, grayImage, INPUT_FILENAME, IMAGE_SIZE_Y, IMAGE_SIZE_X, imageBE);
	case rgblong: return readrgb(imagergblong, grayImage, INPUT_FILENAME, IMAGE_SIZE_Y, IMAGE_SIZE_X, imageBE);
	case rgbfloat: return readrgb(imagergbfloat, grayImage, INPUT_FILENAME, IMAGE_SIZE_Y, IMAGE_SIZE_X, imageBE);
	case rgbnibyte: return readrgb_noninterlaced(imagergbbyte, grayImage, INPUT_FILENAME, IMAGE_SIZE_Y, IMAGE_SIZE_X, imageBE);
	case rgbnishort: return readrgb_noninterlaced(imagergbshort, grayImage, INPUT_FILENAME, IMAGE_SIZE_Y, IMAGE_SIZE_X, imageBE);
	case rgbnilong: return readrgb_noninterlaced(imagergblong, grayImage, INPUT_FILENAME, IMAGE_SIZE_Y, IMAGE_SIZE_X, imageBE);
	case rgbnifloat: return readrgb_noninterlaced(imagergbfloat, grayImage, INPUT_FILENAME, IMAGE_SIZE_Y, IMAGE_SIZE_X, imageBE);
	case graybyte: return readgray<unsigned char>(grayImage, INPUT_FILENAME, IMAGE_SIZE_Y, IMAGE_SIZE_X, imageBE);
	case grayshort: return readgray<unsigned short>(grayImage, INPUT_FILENAME, IMAGE_SIZE_Y, IMAGE_SIZE_X, imageBE);
	case graylong: return readgray<unsigned long>(grayImage, INPUT_FILENAME, IMAGE_SIZE_Y, IMAGE_SIZE_X, imageBE);
	case grayfloat: 
	default: return readgray<float>(grayImage, INPUT_FILENAME, IMAGE_SIZE_Y, IMAGE_SIZE_X, imageBE);
	}
}
bool writeimage()
{
	switch (imageType)
	{
	case rgbbyte: return writergb(imagergbbyte, grayImage, OUTPUT_FILENAME, imageBE);
	case rgbshort: return writergb(imagergbshort, grayImage, OUTPUT_FILENAME, imageBE);
	case rgblong: return writergb(imagergblong, grayImage, OUTPUT_FILENAME, imageBE);
	case rgbfloat: return writergb(imagergbfloat, grayImage, OUTPUT_FILENAME, imageBE);
	case rgbnibyte: return writergb_noninterlaced(imagergbbyte, grayImage, OUTPUT_FILENAME, imageBE);
	case rgbnishort: return writergb_noninterlaced(imagergbshort, grayImage, OUTPUT_FILENAME, imageBE);
	case rgbnilong: return writergb_noninterlaced(imagergblong, grayImage, OUTPUT_FILENAME, imageBE);
	case rgbnifloat: return writergb_noninterlaced(imagergbfloat, grayImage, OUTPUT_FILENAME, imageBE);
	case graybyte: return writegray<unsigned char>(grayImage, OUTPUT_FILENAME, imageBE);
	case grayshort: return writegray<unsigned short>(grayImage, OUTPUT_FILENAME, imageBE);
	case graylong: return writegray<unsigned long>(grayImage, OUTPUT_FILENAME, imageBE);
	case grayfloat: 
	default: return writegray<float>(grayImage, OUTPUT_FILENAME, imageBE);
	}
}

static double relshiftx[] = {0.5, -.25, 0.5, 0}, relshifty[] = {0.5, .25, 0, 0.25}; 
void relativeshift(double & relshift_x, double & relshift_y, int number)
{
	relshift_x=relshift_y=1+EPSI; 
	for (int i=0, j=1; number!=0; i++, j*=1+(i/4)*3, i=i%4, number>>=1)
		if ((number & 1) !=0)
			relshift_x += relshiftx[i]/j, relshift_y += relshifty[i]/j; 
	relshift_x = relshift_x-(int)relshift_x, relshift_y = relshift_y-(int)relshift_y;  
}
void histmatr::reset(int picsizex_, int picsizey_, int histsize_, double histlimit_felso, double histlimit_also, int terfrek_ism_, int areasize_) 
{ 
	picsizex=picsizex_; 
	picsizey=picsizey_; 
	areasize=areasize_; 

int sx, sy; 

	cornerx=cornery=0; 

	sx = (picsizex+cornerx+areasize-1)/areasize+1;
	sy = (picsizey+cornery+areasize-1)/areasize+1; 

	cornerx = (picsizex-(sx-1)*areasize)/2; 
	cornery = (picsizey-(sy-1)*areasize)/2; 

double relshift_x, relshift_y; 
	relativeshift(relshift_x, relshift_y, terfrek_ism_); 

	cornerx += (int)(relshift_x*areasize); 
	if (cornerx>0) cornerx-=areasize; 
	cornery += (int)(relshift_y*areasize); 
	if (cornery>0) cornery-=areasize; 

	sx = (picsizex-cornerx+areasize-1)/areasize+1;
	sy = (picsizey-cornery+areasize-1)/areasize+1; 

	mhist.resize(sx,sy); 
	for (int i=0; i<mhist.getsizex(); i++) for (int j=0; j<mhist.getsizey(); j++) 
		mhist(i,j).reset(histsize_, histlimit_felso, histlimit_also); 
} 

void histogram::equalize() 
{ 
int i, jmin, jmax, valt, k;
double numberin, dmax, dmin, cum; 
double *levelsP1=&(levels[1]);

	for (i=0, numberin=0; i<cells.size; i++) 
		numberin+=cells[i];
	if (numberin==0) 
	{
		if (cells.size==1)
			levelsP1[0]=0;
		else
		{
			for (i=0; i<cells.size; i++) 
				levelsP1[i]=cells[i]/(cells.size-1.);
		}

		return; 
	}
	else
	{
		for (i=0; i<cells.size; i++) 
			levelsP1[i]=cells[i]/numberin; // levelsP1 : most segedtomb 
	}

	dmax = ratioofMAX/cells.size; 
	dmin = ratioofMIN/cells.size; 

	for (k=0; k<4; k++)
	{
		for (i=jmin=jmax=0, valt=0, cum=0; i<cells.size; i++) 
		{ 
		double c=levelsP1[i]; 
			if (c<dmin) 
			{ levelsP1[i]=dmin; valt=1; }
			else if (c>dmax) 
			{ levelsP1[i]=dmax; valt=1; }

			cum+=c-levelsP1[i]; 

			if (levelsP1[i]==dmin) 
			{ jmin++; }
			else if (levelsP1[i]==dmax) 
			{ jmax++; }
//			else // if (levelsP1[i]>dmin && levelsP1[i]<dmax)
//			{ jmid++; }
		} 

		if (valt==0) // nem volt valtoztatas
			break; 
		else if (jmin+jmax==cells.size)
		{
			if (cum>0)
			{
				for (i=0, cum/=jmin; i<cells.size; i++) 
				{
					if (levelsP1[i]==dmin) 
						levelsP1[i]+=cum; 
				}
			}
			else if (cum<0)
			{
				for (i=0, cum/=jmax; i<cells.size; i++) 
				{
					if (levelsP1[i]==dmax) 
						levelsP1[i]+=cum; 
				}
			}
		}
		else
		{
			for (i=0, cum/=(cells.size-(jmin+jmax)); i<cells.size; i++) 
			{
				if (levelsP1[i]<dmax && levelsP1[i]>dmin) 
					levelsP1[i]+=cum; 
			}
		}
	}

	for (i=0; i<cells.size; i++) 
		levelsP1[i]+=levelsP1[i-1]; 
	for (i=0, cum=levelsP1[cells.size-1]; i<cells.size; i++) 
		levelsP1[i]/=cum; // levelsP1 : itt lett igazi fuggveny 
} 

/////////////////////////////////////////////////////////

void file_to_amclahe(matrix<double> & picin)
{
int i,j; 
	for (i=0; i<picin.getsizex(); i++) for (j=0; j<picin.getsizey(); j++) 
	{ picin(i,j) = file_to_amclahe(picin(i,j)); }
}
void amclahe_to_file(matrix<double> & picin)
{
int i,j; 
	for (i=0; i<picin.getsizex(); i++) for (j=0; j<picin.getsizey(); j++) 
	{ picin(i,j) = amclahe_to_file(picin(i,j)); }
}
// #endif

void amclahe(matrix<double> & pic, // out, const matrix<double> & picin, 
				const pureallocator<double> & histlimit_felso, const pureallocator<double> & histlimit_also, const pureallocator<int> & terfrek_ism, 
				const pureallocator<int> & histsize, const pureallocator<int> & areasize) 
{ 
histmatr mhist; 
	for (int k=0, szaml=0; k<histlimit_felso.getsize(); k++) 
	{ 
		if (histlimit_felso[k]>POSSIBLE_MINIMAL_FELSO_LIMIT &&
			histlimit_also[k]<1./POSSIBLE_MINIMAL_FELSO_LIMIT)
		{
			for (int kk=0; kk<terfrek_ism[k]; kk++, szaml++) 
			{ 
				mhist.reset(pic.getsizex(), pic.getsizey(), histsize[k], histlimit_felso[k], histlimit_also[k], kk, areasize[k]);
				mhist.put(pic);
				mhist.extend();
				mhist.equalize();
				mhist.traf(pic, pic);
			}
		}
	} 
}
static int set_terfrek_ism(double & histlimit_felso, double & histlimit_also)
{
int ret=1;
double x=max(histlimit_felso/MAX_CONTRAST_LIMIT_UPPER_PER_STEP, MIN_CONTRAST_LIMIT_UPPER_PER_STEP/(histlimit_also+EPS));
	if (x<=1)
		return ret;
int f=(int)(log(histlimit_felso+EPS*EPS)/log(MAX_CONTRAST_LIMIT_UPPER_PER_STEP)+1-EPS);
int l=(int)(log(histlimit_also+EPS*EPS)/log(MIN_CONTRAST_LIMIT_UPPER_PER_STEP)+1-EPS);
	ret=max(f,l);
	histlimit_felso=exp(log(histlimit_felso)/ret);
	histlimit_also=exp(log(histlimit_also+EPS)/ret);
	return ret;
}
static void set_terfrek_ism(pureallocator<int> & terfrek_ism, pureallocator<double> & histlimit_felso, pureallocator<double> & histlimit_also)
{
	for (int i=0; i<histlimit_felso.getsize(); i++) // ==histlimit_also.getsize()
		terfrek_ism(i)=set_terfrek_ism(histlimit_felso(i), histlimit_also(i));
}
void amclahe(matrix<double> & pic, pureallocator<double> & histlimit_felso, pureallocator<double> & histlimit_also)
{ 
pureallocator<int> terfrek_ism(histlimit_felso.getsize()), histsize(histlimit_felso.getsize()), areasize(histlimit_felso.getsize());  
	set_terfrek_ism(terfrek_ism, histlimit_felso, histlimit_also);

	areasize[0]=max(pic.getsizex(), pic.getsizey()); 
int k, kk; 
	for (k=1; k<histlimit_felso.getsize(); k++) 
	{ 
		areasize[k]=areasize[k-1]/2; 
		if (areasize[k]<(minimal_areasize+1)/2)
			break;
	}
	histlimit_felso.resize(k); histlimit_also.resize(k); terfrek_ism.resize(k); areasize.resize(k); histsize.resize(k); 
	for (kk=0; kk<k; kk++) 
		histsize[kk]=2*areasize[kk]; 

	amclahe(pic, histlimit_felso, histlimit_also, terfrek_ism, histsize, areasize); 
} 
void amclahe(matrix<double> & pic, pureallocator<pureallocator<double> > & histlimit_felso, pureallocator<pureallocator<double> > & histlimit_also)
{ 
	for (int i=0; i<histlimit_felso.getsize(); i++)
		amclahe(pic, histlimit_felso(i), histlimit_also(i));
}
void amclahe(matrix<double> & pic)
{
	if (sequenceWeight.getsize()==1)
	{
		amclahe(pic, sequenceUpperLimit(0), sequenceLowerLimit(0));
		return;
	}
matrix<double> pic1(pic.getsizex(), pic.getsizey(), 0);
	for (int i=0; i<sequenceWeight.getsize(); i++)
	{
	matrix<double> pic2=pic;
		amclahe(pic2, sequenceUpperLimit(i), sequenceLowerLimit(i));
		pic2*=sequenceWeight(i);
		pic1+=pic2;
	}
	pic=pic1;
}

///////////////////////////////////////////////////////////////

inline bool nokeyword(const char* keyword) 
{ printf("\t no data of    %s    acceptable", keyword); return false; }
inline bool incorrecttag_(const char* keyword, const char* tag) 
{ printf("\ttag   %s  at keyword   %s   is incorrect", keyword, tag); return false; }
#define incorrecttag(keyword) incorrecttag_(keyword, t.getTag())
 
bool inputFromParfile(const char *parfilename)
{
textinput t(parfilename);
	if (! t.isOpen())
		return false;

	if (! t.getLine("INPUT_FILENAME") || 0==strcmp("",t.getNextTag()))
		return nokeyword("INPUT_FILENAME");
	strcpy(INPUT_FILENAME, t.getTag());

	if (! t.getLine("HEADER_LENGTH") || 0==strcmp("",t.getNextTag()))
		HEADER_LENGTH=0;
	else 
		HEADER_LENGTH=atoi(t.getTag());

	if (! t.getLine("IMAGE_SIZE"))
		return nokeyword("IMAGE_SIZE");
	if (0==strcmp("",t.getNextTag()))
		return nokeyword("IMAGE_SIZE");
	IMAGE_SIZE_Y=atoi(t.getTag());
	if (0==strcmp("",t.getNextTag()))
		return nokeyword("IMAGE_SIZE");
	IMAGE_SIZE_X=atoi(t.getTag());

bool itypergbi=false, itypergbni=false, itypegray=false; 
	if (! t.getLine("IMAGE_TYPE"))
		return nokeyword("IMAGE_TYPE");
	t.getNextTag();
	if (0==strcmp(t.getTag(), "RGB_I"))
		itypergbi=true;
	else if (0==strcmp(t.getTag(), "RGB_NI"))
		itypergbni=true;
	else if (0==strcmp(t.getTag(), "GRAY"))
		itypegray=true;
	else
		return incorrecttag("IMAGE_TYPE");

bool channeli8=false, channeli16=false, channeli32=false, channelfloat=false; 
	if (! t.getLine("CHANNEL_TYPE"))
		return nokeyword("CHANNEL_TYPE");
	t.getNextTag();
	if (0==strcmp(t.getTag(), "INT8"))
		channeli8=true;
	else if (0==strcmp(t.getTag(), "INT16"))
		channeli16=true;
	else if (0==strcmp(t.getTag(), "INT32"))
		channeli32=true;
	else if (0==strcmp(t.getTag(), "FLOAT"))
		channelfloat=true;
	else
		return incorrecttag("CHANNEL_TYPE");

	imageType = (itypergbi && channeli8)? rgbbyte:
							(itypergbi && channeli16)? rgbshort:
							(itypergbi && channeli32)? rgblong:
							(itypergbi && channelfloat)? rgbfloat:
							(itypergbni && channeli8)? rgbnibyte:
							(itypergbni && channeli16)? rgbnishort:
							(itypergbni && channeli32)? rgbnilong:
							(itypergbni && channelfloat)? rgbnifloat:
							(itypegray && channeli8)? graybyte:
							(itypegray && channeli16)? grayshort:
							(itypegray && channeli32)? graylong:
							(itypegray && channelfloat)? grayfloat:
							grayfloat;

std::cerr << "Break 1 reading Par file!" << std::endl;

	if (! t.getLine("ENDIAN"))
		return nokeyword("ENDIAN");
	t.getNextTag();

std::cerr << "Break 1.2 reading Par file!" << std::endl;

	if (0==strcmp(t.getTag(), "LITTLE"))
		imageBE=false;
	else if (0==strcmp(t.getTag(), "BIG"))
		imageBE=true;
	else 
		return incorrecttag("ENDIAN");

std::cerr << "Break 1.3 reading Par file!" << std::endl;

	if (! t.getLine("ROW_END_DUMMY_BYTES_NUMBER_MOD") || 0==strcmp("",t.getNextTag()))
		ROW_END_DUMMY_BYTES_NUMBER_MOD=0;
	else 
		ROW_END_DUMMY_BYTES_NUMBER_MOD=atoi(t.getTag());

std::cerr << "Break 1.5 reading Par file!" << std::endl;

	if (ROW_END_DUMMY_BYTES_NUMBER_MOD==0 || ROW_END_DUMMY_BYTES_NUMBER_MOD==1)
		ROW_END_DUMMY_BYTES_NUMBER=0;
	else 
	{
	unsigned int unit, rmod=ROW_END_DUMMY_BYTES_NUMBER_MOD;
		switch (imageType)
		{
		case rgbbyte: unit=3; break;
		case rgbshort: unit=6; break;
		case rgblong: unit=12; break;
		case rgbfloat: unit=12; break;
		case rgbnibyte: unit=1; break;
		case rgbnishort: unit=2; break;
		case rgbnilong: unit=4; break;
		case rgbnifloat: unit=4; break;
		case graybyte: unit=1; break;
		case grayshort: unit=2; break;
		case graylong: unit=4; break;
		case grayfloat: 
		default: unit=4; break;
		}
		ROW_END_DUMMY_BYTES_NUMBER = (rmod-(unit*IMAGE_SIZE_Y)%rmod)%rmod;
	}

std::cerr << "Break 2 reading Par file!" << std::endl;

	if (! t.getLine("INPUT_BRIGHTEST") || 0==strcmp("",t.getNextTag()))
		INPUT_BRIGHTEST=channeli8?(unsigned char)(-1):
										channeli16?(unsigned short)(-1):
										channeli32?(unsigned long)(-1):
										channelfloat?1:
										1;
	else 
		INPUT_BRIGHTEST=atof(t.getTag());

	if (! t.getLine("INPUT_MEDIA_CONTRAST") || 0==strcmp("",t.getNextTag()))
		INPUT_MEDIA_CONTRAST=channeli8?(unsigned char)(-1)/1.:
													channeli16?(unsigned short)(-1)/1.:
													channeli32?(unsigned long)(-1)/1.:
													channelfloat?1e20:
													1e20;
	else 
		INPUT_MEDIA_CONTRAST=atof(t.getTag());

std::cerr << "Break 3 reading Par file!" << std::endl;


	if (! t.getLine("CONTRAST_CLIPPING_OF_AREA_RATIO_LOWER") || 0==strcmp("",t.getNextTag()))
		CONTRAST_CLIPPING_OF_AREA_RATIO_LOWER=0;
	else 
		CONTRAST_CLIPPING_OF_AREA_RATIO_LOWER=atof(t.getTag());

	if (! t.getLine("CONTRAST_CLIPPING_OF_AREA_RATIO_UPPER") || 0==strcmp("",t.getNextTag()))
		CONTRAST_CLIPPING_OF_AREA_RATIO_UPPER=0;
	else 
		CONTRAST_CLIPPING_OF_AREA_RATIO_UPPER=atof(t.getTag());

	if (! t.getLine("OUTPUT_BRIGHTEST") || 0==strcmp("",t.getNextTag()))
		OUTPUT_BRIGHTEST=channeli8?(unsigned char)(-1):
										channeli16?(unsigned short)(-1):
										channeli32?(unsigned long)(-1):
										channelfloat?1:
										1;	else 
		OUTPUT_BRIGHTEST=atof(t.getTag());

	if (! t.getLine("OUTPUT_MEDIA_CONTRAST") || 0==strcmp("",t.getNextTag()))
		OUTPUT_MEDIA_CONTRAST=50;
	else 
		OUTPUT_MEDIA_CONTRAST=atof(t.getTag());

	if (! t.getLine("MAX_CONTRAST_LIMIT_UPPER_PER_STEP") || 0==strcmp("",t.getNextTag()))
		MAX_CONTRAST_LIMIT_UPPER_PER_STEP=BIGNUM;
	else 
		MAX_CONTRAST_LIMIT_UPPER_PER_STEP=atof(t.getTag());

	if (! t.getLine("MIN_CONTRAST_LIMIT_UPPER_PER_STEP") || 0==strcmp("",t.getNextTag()))
		MIN_CONTRAST_LIMIT_UPPER_PER_STEP=1./MAX_CONTRAST_LIMIT_UPPER_PER_STEP;
	else 
		MIN_CONTRAST_LIMIT_UPPER_PER_STEP=atof(t.getTag());

	if (! t.getLine("OUTPUT_FILENAME") || 0==strcmp("",t.getNextTag()))
		strcpy(OUTPUT_FILENAME, INPUT_FILENAME);
	else
		strcpy(OUTPUT_FILENAME, t.getTag());

	if (! t.getFirstLine() || 0!=strcmp("SEQUENCE_WEIGHT", t.getTag()) && ! t.getLine("SEQUENCE_WEIGHT"))
		return nokeyword("SEQUENCE_WEIGHT");


std::cerr << "Half reading Par file!" << std::endl;


int section, subsection, item;
	for (section=0;;section++)
	{
//	double & sequenceWeight_ = sequenceWeight.add()=atof(t.getNextTag());
	pureallocator<pureallocator<double> > & sequenceUpperLimit_ = sequenceUpperLimit.add();
	pureallocator<pureallocator<double> > & sequenceLowerLimit_ = sequenceLowerLimit.add();
		for (subsection=0; ;)
		{
			if (! t.getNextLine())
			{
				if (sequenceUpperLimit_.last().getsize()==0 && sequenceLowerLimit_.last().getsize()==0)
				{ sequenceUpperLimit.erase(); sequenceLowerLimit.erase(); sequenceWeight.erase(); }
				if (sequenceWeight.getsize()==0)
				{ printf("\t no data concerning limits"); return false; }
				goto break2;
			}
		bool upp;
			if (0==strcmp("SEQUENCE_WEIGHT",t.getTag()))
			{
				if (sequenceUpperLimit_.getsize()==0 && sequenceLowerLimit_.getsize()==0)
				{ sequenceUpperLimit.erase(); sequenceLowerLimit.erase(); sequenceWeight.erase(); }
				break;
			}
			else if (0==strcmp("HISTLIMIT_UPPER",t.getTag()))
				upp=true;
			else if (0==strcmp("HISTLIMIT_LOWER",t.getTag()))
				upp=false;
			else
			{
				printf("\tkeyword   %s   is incorrect", t.getTag());
				return false;
			}
			if (! upp && (subsection==0 || sequenceUpperLimit_(subsection-1).getsize()==0))
			{
				printf("\tkeyword   HISTLIMIT_LOWER    is at wrong place");
				return false;
			}
			if (upp)
			{ sequenceUpperLimit_.add(); sequenceLowerLimit_.add(); subsection++; }
		pureallocator<double> & seq = upp?sequenceUpperLimit_.last():sequenceLowerLimit_.last();
			seq.resize(0);
			for (;0!=strcmp("",t.getNextTag());)
				seq.add(atof(t.getTag()));
			if (upp)
			{
			pureallocator<double> & seq = sequenceLowerLimit_.last()=sequenceUpperLimit_.last();
				for (item=0; item<seq.getsize(); item++)
					seq(item)=1./seq(item);
			}
		}
	}
break2:
double w;
	for (section=0, w=0; section<sequenceWeight.getsize(); section++)
		w+=sequenceWeight(section);
	for (section=0; section<sequenceWeight.getsize(); section++)
	for (sequenceWeight(section)/=w, subsection=0; subsection<sequenceUpperLimit(section).getsize(); subsection++)
	{
	int s=min(sequenceUpperLimit(section)(subsection).getsize(), sequenceLowerLimit(section)(subsection).getsize()); 
		sequenceUpperLimit(section)(subsection).resize(s);
		sequenceLowerLimit(section)(subsection).resize(s);
	}

	return true;
}
void clipAndSet(matrix<double> & pic)
{
double brightest=INPUT_BRIGHTEST, darkest=INPUT_BRIGHTEST/INPUT_MEDIA_CONTRAST;
int i,j; 

	if (CONTRAST_CLIPPING_OF_AREA_RATIO_UPPER<EPS && CONTRAST_CLIPPING_OF_AREA_RATIO_LOWER<EPS)
	{
		input_lower_clipping=darkest; 
		input_upper_clipping=brightest; 
	}
	else
	{
	int numberofelements=pic.getsizex()*pic.getsizey(); 
	int contrast_clipping_by_area_upper_ = (int)ceil(max(0.,CONTRAST_CLIPPING_OF_AREA_RATIO_UPPER)*numberofelements); 
	int contrast_clipping_by_area_lower_ = (int)ceil(max(0.,CONTRAST_CLIPPING_OF_AREA_RATIO_LOWER)*numberofelements); 

	double histsize_eps=histsize_at_clipping-1e-2;
	vector<int> hist(histsize_at_clipping, 0); 

		for (i=0; i<pic.getsizex(); i++) for (j=0; j<pic.getsizey(); j++) 
			hist[(int)(_ab_to_01_clip(pic(i,j),darkest,brightest)*histsize_eps)]++; 

	int lower_clipping, upper_clipping, sumofitems; 

		for (lower_clipping=0, sumofitems=hist[lower_clipping]; 
				lower_clipping<histsize_at_clipping; 
				lower_clipping++, sumofitems+=hist[lower_clipping])
			if (sumofitems>contrast_clipping_by_area_lower_) 
				break; 

	//	lower_clipping=++; // tobbet vag, sakktablat eltunteti

		for (upper_clipping=histsize_at_clipping-1, sumofitems=hist[upper_clipping]; 
				upper_clipping>=0; 
				upper_clipping--, sumofitems+=hist[upper_clipping])
			if (sumofitems>contrast_clipping_by_area_upper_) 
				break; 

	//	upper_clipping=--; // tobbet vag, sakktablat eltunteti

			if (upper_clipping<=lower_clipping)
			{
				if (lower_clipping==histsize_at_clipping-1)
					lower_clipping=upper_clipping-1; 
				else
					upper_clipping=lower_clipping+1; 
			}

		input_lower_clipping=lower_clipping/(histsize_at_clipping-1.); 
		input_upper_clipping=upper_clipping/(histsize_at_clipping-1.); 
	}

	for (i=0; i<pic.getsizex(); i++) for (j=0; j<pic.getsizey(); j++) 
	{
		if (pic(i,j)<input_lower_clipping)
			pic(i,j)=input_lower_clipping; 
		if (pic(i,j)>input_upper_clipping)
			pic(i,j)=input_upper_clipping; 
	}

	output_lower_clipping=OUTPUT_BRIGHTEST/OUTPUT_MEDIA_CONTRAST; 
	output_upper_clipping=OUTPUT_BRIGHTEST; 
	log_input_lower_clipping=log(input_lower_clipping);
	log_input_upper_clipping=log(input_upper_clipping);
	log_output_lower_clipping=log(output_lower_clipping);
	log_output_upper_clipping=log(output_upper_clipping);
}

bool workFromParfile(const char *parfilename)
{
std::cerr << "Before everything!" << std::endl;

	if (! inputFromParfile(parfilename))
	{
std::cerr << "Read Par File Error!" << std::endl;
		return false;
        }

std::cerr << "Read Par File Ok!" << std::endl;

	if (! readimage())
		return false;

	clipAndSet(grayImage);


matrix<double> & pic=grayImage; // & // ***

std::cerr << " Read pic" << std::endl;

	file_to_amclahe(pic);

	amclahe(pic);

std::cerr << " Algorithm" << std::endl;

	amclahe_to_file(pic);

std::cerr << " Write pic" << std::endl;

//	grayimage=pic; // ***
	if (! writeimage())
		return false;
	return true;
}
