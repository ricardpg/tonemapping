//#define __BEOLV_CPP__

#include "beolv.h"

namestring iodummy; 

///////////////////////////////////////////////////////////////////////

STATIC_MEMBER char textinput::commentCharacter EQUALS '#';

char* toupper(char* ret, const char* str)
{
	strcpy(ret, str);
char *c;
	for (c=ret; *c!='\0'&&c-ret<100000; c++)
//		if (isalpha(*c))
		*c=(char)toupper(*c);
	return ret;
}
const char* toupper(const char* str)
{
static namestring temp; 
	return toupper(temp, str);
}
char* tolower(char* ret, const char* str)
{
	strcpy(ret, str);
char *c;
	for (c=ret; *c!='\0'&&c-ret<100000; c++)
//		if (isalpha(*c))
		*c=(char)tolower(*c);
	return ret;
}
const char* tolower(const char* str)
{
static namestring temp; 
	return tolower(temp, str);
}

///////////////////////////////////////////////////////////////////////

bool textinput::getNextLine() // closes file if false // tag(0): toupper, currentTag=0
{
	reset();
	if (! isOpen())
	{
		close();
		return false;
	}
textinput_getNextLine_start:
	line.resize(0);
	for (;;)
	{
	char ch=line.add()=getc(file);
		if (ch==EOF || ch=='\n')
			break;
	}
	if (line(0)==EOF)
	{
		close();
		return false;
	}
	line.last()='\0';
char *c = strchr(&(line(0)),commentCharacter);
	if (c != NULL)
	{
		*c='\0';
		line.resize(c-&(line(0))+1);
	}
	c=&(line(0));
	if (*c != '\0')
		while (*c==' ' || *c=='\t')
			c++;
	if (*c=='\0')
		goto textinput_getNextLine_start;
	else
	{
	int i=c-&(line(0));
		if (i>0)
			for (int j=0; j<line.getsize()-i; j++)
				line(j)=line(j+i);
//		return true;
	}

	tag.resize(0);
	for (c=&(line(0));;)
	{
		tag.add()=c;
		while (*c!=' ' && *c!='\t' && *c!='\0')
			c++;
		if (*c=='\0')
			break;
		*c='\0';
		c++;
		while (*c==' ' || *c=='\t')
			c++;
		if (*c=='\0')
			break;
	}
	tag.add()=c;

	if (upper)
		toupper();

	return true;
}
bool textinput::getLine(const char* keyword)
{
	while (getNextLine())
		if (0==strcmp(getTag(), upper?::toupper(keyword):keyword))
			return true;
	if (! getFirstLine())
		return false;
	if (0==strcmp(getTag(), upper?::toupper(keyword):keyword))
		return true;
	while (getNextLine())
		if (0==strcmp(getTag(), upper?::toupper(keyword):keyword))
			return true;
	return false;
}

