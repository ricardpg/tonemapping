#ifndef __BASICS_H__
#define __BASICS_H__

#include <iostream>
#include <fstream>

#include <cstdlib>
#include <cstdio>
#include <cstring>

#include <cmath>

//#include <time.h>
//#include <conio.h>
//#include <io.h>
//#include <dos.h>

#ifndef DEBUG
#define DEBUG
#endif

#define BIGNUM 1.e16
#define EPS 1.e-13
#define EPSI 1.1e-10
#define EPS3 1e-3
#define EPS4 1e-4
#define EPS5 1e-5
#define DELTA 1e-2

#define PI	3.14159265359

#define RECIP(x) (1./((x)+1.e-40))
#define SQRT(x) sqrt((x)+1.e-40)
#define POW(x,y) pow((x)+1.e-40,y)

#ifndef max
template <class T> inline T max(const T & x, const T & y) { return ((x)>(y)?(x):(y)); }
#endif
#ifndef min
template <class T> inline T min(const T & x, const T & y) { return ((x)<(y)?(x):(y)); }
#endif
#ifndef abs
template <class T> inline T abs(const T & x) { return ((x)<0?-(x):(x)); }
#endif
#ifndef sgn
template <class T> inline int sgn(const T & x) { return ((x)<0?-1:(x)>0?1:0); }
#endif
#ifndef swap
template <class T> inline void swap(T & x, T & y) { T temp=x; x=y; y=temp; }
#endif

#if ! defined (BOOL) && ! defined (_WINDEF_)
#define BOOL int
#endif
#if ! defined (FALSE) && ! defined (_WINDEF_)
#define FALSE 0
#endif
#if ! defined (TRUE) && ! defined (_WINDEF_)
#define TRUE 1
#endif

#if ! defined (NULL) && ! defined (_WINDEF_)
#define NULL 0
#endif

typedef unsigned char uchar;
typedef unsigned char BYTE;
typedef unsigned short ushort;
typedef unsigned int uint; 
typedef unsigned long ulong; 
typedef long double ldouble; 

typedef unsigned short WORD; 
typedef unsigned long DWORD; 
typedef unsigned short UINT1; 
typedef long LONG; 

inline int mod(int a, int b) { int ret=a%b; return (ret==0||a>0?ret:b+ret); }

void CancelPrintf(); 
BOOL Canceled(); 

#ifdef DEBUG
// #include <conio.h>
inline void myassert(const char* str1, int i2)
{ printf("\n\n%s:%d\npress any key to continue\n", str1, i2); //getch(); 
exit (1); }
inline void myassert(const char* str1)
{ printf("\n\n%s\npress any key to continue\n", str1); //getch(); 
exit (1); }
#else
#define myassert(str1, i2) 
#define myassert(str1) 
#endif
inline BOOL debug() { return (FALSE); }

///////////////////////////////////////////////////////////////////////////////////////////////////

// file Text/Binary
/*
#define CommentLineChar '#'
#define MaxLengthOfFileItem 5000
#define defaultRealFormat "%9.3f"
*/

#undef SARGA_BARNA_KORLATOZAS_ERTEK_SZERINT

#define NAMESTRING_MAXLENGTH 260
#define LONGNAMESTRING_MAXLENGTH 1000

typedef char namestring[NAMESTRING_MAXLENGTH];
typedef char longnamestring[LONGNAMESTRING_MAXLENGTH];

extern namestring iodummy; 

///////////////////////////////////////////////////////////////////////////////////////////////////

#define BEGIN_CDECL extern "C" {
#define END_CDECL }

#define SCALAR double

#endif // __BASICS_H__

