// MANUAL templates for array functions 
// MANUAL ...features of templates (which is which): size, dynamic reallocation, economic reallocation, arithmetic operators

#ifndef __VECTOR_H__
#define __VECTOR_H__

#include <iostream>

#include "basegeom.h" 

template <class T> class ptrNsize; 
template <class T> class purevector; 
template <class T> class vector; 
template <class T> class pureallocator; 
template <class T> class allocator; 

// MANUAL sized array with overloaded functionalities (as expected); no allocation, so it can be reference like, no real destruction
template <class T>
class ptrNsize 
{
public:
	T *elem;
//protected:
	int size; 
public:
	inline ptrNsize(int _size=0, T* _elem=NULL) { set(_size, _elem); }
	inline ptrNsize(const ptrNsize<T> &p) { set(p.size, p.elem); }
	inline void set(int _size=0, T* _elem=NULL) { size = _size; elem = _elem; }
	inline ptrNsize & operator =(const ptrNsize<T> & p) { set(p.size, p.elem); return (*this); }
	inline ptrNsize & operator =(const T & initial) { for (int i=0; i<size; i++) (*this)(i) = initial; return (*this); }
	inline void swap(ptrNsize<T> & p) { ::swap(size, p.size); ::swap(elem,p.elem); }
	inline ~ptrNsize() { }
	inline int getsize() const { return size; }
	inline void resize(const int newsize) { size=newsize; }
	inline void reset() { resize(0); }
	inline const T & operator ()(const int i) const 
	{
		#ifdef _DEBUG
			if (i>=size || i<0) 
				throw "overindexed ptrNsize";
		#endif
		return elem[i];
	}
	inline T & operator ()(const int i) 
	{ 
		#ifdef _DEBUG
			if (i>=size || i<0) 
				throw "overindexed ptrNsize";
		#endif
		return elem[i];
	}
	inline const T & operator [](const int i) const { return operator()(i); }
	inline T & operator [](const int i) { return operator()(i); }
	inline const T & last() const { return operator()(getsize()-1); }
	inline T & last() { return operator()(getsize()-1); }
};

template <class T>
inline std::ostream & operator<< (std::ostream & os, const ptrNsize<T> & v)
{ 
//	indent;
	os << std::endl << "( size : " << v.getsize() << " ,  elem : ";
	{
		////indent;
		os << "  ";
		for (int i=0; i<v.getsize(); i++)
		{
			os << std::endl << "( " << i << " : ";
			{
				////indent;
				os << "  ";
				os << v(i);
			}
			os  << " ) ";
		}
	}
	return os << ") ";
}
template <class T>
inline std::istream & operator>> (std::istream & is, ptrNsize<T> & v) // veszélyes !!!
{ 
int size, ind;
	is >> iodummy >> iodummy >> iodummy >> size >> iodummy >> iodummy >> iodummy;
	v.reset();
	v.resizeJust(size);
	for (int i=0; i<v.getsize(); i++)
	{
		is >> iodummy >> ind;
		is >> iodummy >> v(ind) >> iodummy;
	}
	return is >> iodummy;
}

template class ptrNsize<double>;
template class ptrNsize<ldouble>;
template class ptrNsize<vector3<double> >;
template class ptrNsize<pureallocator<vector3<double> > > ;

// MANUAL ...ptrNsize with allocation, contructors, operator=, reallocation, destruction, etc (as expected), no arithmetics 
template <class T>
class purevector : public ptrNsize<T>
{
public:
	inline purevector(int _size) : ptrNsize<T> (_size, new T[max(_size,0)]) { } // : ptrNsize<T> (_size, new T[max(_size,1)]) { }
	inline purevector(int _size, const T & initial) : ptrNsize<T> (_size, new T[max(_size,0)]) // : ptrNsize<T> (_size, new T[max(_size,1)]) 
	{ this->operator =(initial); }
	inline purevector() : ptrNsize<T> (0, new T[0]) { } // : ptrNsize<T> (0, new T[1]) { } // { size = 0; elem = new T[1]; }
	inline purevector(const ptrNsize<T> &p) : ptrNsize<T> (p.getsize(), new T[max(0,p.getsize())]) // : ptrNsize<T> (p.getsize(), new T[max(1,p.getsize())]) 
	{
		for (int i=0; i<ptrNsize<T>::size; i++) 
		{
//			T temp = p(i);
//			(*this)(i) = temp;
			(*this)(i) = p(i);
		}
	}
	inline purevector(const purevector<T> &p) : ptrNsize<T> (p.getsize(), new T[max(0,p.getsize())]) // : ptrNsize<T> (p.getsize(), new T[max(1,p.getsize())]) 
	{
		for (int i=0; i<ptrNsize<T>::size; i++) 
		{
//			T temp = p(i);
//			(*this)(i) = temp;
			(*this)(i) = p(i);
		}
	}		
	inline purevector & operator =(const purevector<T> & p) 
	{
		if (this != &p) 
		{
			resize(p.getsize());
			for (int i=0; i<ptrNsize<T>::size; i++) 
			{
//				T temp = p(i);
//				(*this)(i) = temp;
				(*this)(i) = p(i);
			}
		}
		return (*this);
	}
	template <class TT>
	inline void set(const purevector<TT> & p) 
	{
		if (this != &p) 
		{
			resize(p.getsize());
			for (int i=0; i<ptrNsize<T>::size; i++) 
			{
//				T temp = p(i);
//				(*this)(i) = temp;
				(*this)(i) = (T)(p(i));
			}
		}
	}
	inline purevector & operator =(const T & initial) 
	{
		for (int i=0; i<ptrNsize<T>::size; i++) 
		{
			(*this)(i) = initial;
		}
		return (*this);
	}
/*
	inline void swap(purevector<T> & p) 
	{
		::swap(size, p.size);
		::swap(elem, p.elem);
	}
*/
	inline ~purevector() { if (ptrNsize<T>::elem) { delete []ptrNsize<T>::elem; ptrNsize<T>::elem = NULL; } }
	inline void resize(const int newsize) 
	{ 
		if (ptrNsize<T>::size == newsize) return;
		T *newelem = new T[max(0,newsize)]; // new T[max(newsize,1)];
		int i, to = min(ptrNsize<T>::size, newsize);
		for (i=0; i<to; i++) 
			newelem[i] = (*this)(i);
		if ( ptrNsize<T>::elem ) { delete []ptrNsize<T>::elem; ptrNsize<T>::elem = NULL; }
		ptrNsize<T>::elem = newelem;
		ptrNsize<T>::size = newsize;
	}
	inline void resize(const int newsize, const T & initial) 
	{ 
		if (ptrNsize<T>::size == newsize) return;
		T *newelem = new T[max(0,newsize)]; // new T[max(newsize,1)];
		int i, to = min(ptrNsize<T>::size, newsize);
		for (i=0; i<to; i++) 
			newelem[i] = (*this)(i);
		for (; i<newsize; i++) 
			newelem[i] = initial;
		if ( ptrNsize<T>::elem ) { delete []ptrNsize<T>::elem; ptrNsize<T>::elem=NULL; }
		ptrNsize<T>::elem = newelem;
		ptrNsize<T>::size = newsize;
	}
	inline void reset() 
	{ resize(0); }
};

template <class T>
inline std::ostream & operator<< (std::ostream & os, const purevector<T> & v)
{ 
//	indent;
	os << std::endl << "( size : " << v.getsize() << " ,  elem : ";
	{
		////indent;
		os << "  ";
		for (int i=0; i<v.getsize(); i++)
		{
			os << std::endl << "( " << i << " : ";
			{
				////indent;
				os << "  ";
				os << v(i);
			}
			os  << " ) ";
		}
	}
	return os << ") ";
}
template <class T>
inline std::istream & operator>> (std::istream & is, purevector<T> & v)
{ 
int size, ind;
	is >> iodummy >> iodummy >> iodummy >> size >> iodummy >> iodummy >> iodummy;
	v.reset();
	v.resize(size);
	for (int i=0; i<v.getsize(); i++)
	{
		is >> iodummy >> ind;
		is >> iodummy >> v(ind) >> iodummy;
	}
	return is >> iodummy;
}

template class purevector<double>;
template class purevector<ldouble>;
template class purevector<vector3<double> >;

///////////////////////////////////////////////////////////////////////////////////////////

// MANUAL ...purevector with arithmetics - drawback: if T cannot have e.g. division, purevector shall be used instead
template <class T>
class vector : public purevector<T> 
{
public:
	inline vector(int _size) : purevector<T>(_size) { }
	inline vector(int _size, const T & initial) : purevector<T>(_size, initial) { }
	inline vector() : purevector<T>() { }
	inline vector(const ptrNsize<T> &p) : purevector<T>(p) { }
	inline vector(const purevector<T> &p) : purevector<T>(p) { }
	inline vector(const vector<T> &p) : purevector<T>(p) { }
	inline vector & operator =(const purevector<T> & p) { purevector<T>::operator=(p); return *this; }
	template <class TT>
	inline void set(const purevector<TT> & p) { purevector<T>::set(p); }
	inline vector & operator =(const T & initial) { purevector<T>::operator=(initial); return *this; }

	inline vector & setminus() 
	{ for (int i=0; i<purevector<T>::getsize(); i++) purevector<T>::elem[i]=-purevector<T>::elem[i]; return *this; }

	inline vector & operator +=(const vector &p)
	{ int s = min(purevector<T>::getsize(),p.getsize()); for (int i=0; i<s; i++) purevector<T>::elem[i]+=p[i]; return *this; }
	inline vector & operator -=(const vector &p)
	{ int s = min(purevector<T>::getsize(),p.getsize()); for (int i=0; i<s; i++) purevector<T>::elem[i]-=p[i]; return *this; }
	inline vector & operator ^=(const vector &p)
	{ int s = min(purevector<T>::getsize(),p.getsize()); for (int i=0; i<s; i++) purevector<T>::elem[i]*=p[i]; return *this; }

	inline vector & operator *=(const T & c) { for (int i=0; i<purevector<T>::getsize(); i++) purevector<T>::elem[i]*=c; return *this; }
	inline vector & operator /=(const T & c) { for (int i=0; i<purevector<T>::getsize(); i++)  purevector<T>::elem[i]/=c; return *this; }
//	inline vector operator -() const { vector ret(*this); return ret.setminus(); }
	inline vector operator +(const vector &p) const { vector ret(*this); ret+=p; return ret; }
	inline vector operator -(const vector &p) const { vector ret(*this); ret-=p; return ret; }
// MANUAL point wise multiplication
	inline vector operator ^(const vector &p) const { vector ret(*this); ret^=p; return ret; }
	inline vector operator *(const T & c) const { vector ret(*this); ret*=c; return ret; }
	inline vector operator /(const T & c) const { vector ret(*this); ret/=c; return ret; }
// MANUAL scalar product of different long vectors 
	inline T operator *(const vector &p) const
	{ int i, s=min(purevector<T>::getsize(),p.getsize()); T ret=0; for (i=0; i<s; i++) ret +=  purevector<T>::elem[i]*p[i]; return ret; }
	inline T norm2 () const { return (operator*(*this)); }
};

template class vector<double>;
template class vector<ldouble>;
//template class vector<vector3<double> >;
//template class vector<vector6<double> >;

///////////////////////////////////////////////////////////////////////////////////////////

// MANUAL ...purevector with economic reallocation - i.e. overallocating, and consuming unused parts in most cases
template <class T>
class pureallocator : public purevector<T> 
{
#define add_container 10
#define multip_container 1.5
protected:
	int used; 
public:
	inline pureallocator(int _used=0) : purevector<T>(_used)
	{ used = _used; }
	inline pureallocator(int _used, const T & initial) : purevector<T>(_used) 
	{ 
		used = _used; 
		this->operator =(initial);
	}
	inline pureallocator(const ptrNsize<T> &p) : purevector<T>(p.size) 
	{
		used = p.getsize();
		for (int i=0; i<used; i++) 
		{
			(*this)(i) = p(i);
		}
	};
/*
	inline pureallocator(const purevector<T> &p) : purevector<T>(p.size) 
	{
		used = p.getsize();
		for (int i=0; i<used; i++) 
		{
			(*this)(i) = p(i);
		}
	};
*/
	inline pureallocator(const pureallocator<T> &p) : purevector<T>(p) 
	{ used = p.used; }
	inline pureallocator & operator =(const T & initial) 
	{
		for (int i=0; i<used; i++) 
		{
			(*this)(i) = initial;
		}
		return (*this);
	}
	inline pureallocator & operator =(const purevector<T> & p) 
	{
		resizeJust(p.getsize());
		for (int i=0; i<used; i++) 
		{
			(*this)(i) = p(i);
		}
		return (*this);
	}
	template <class TT>
	inline void set(const purevector<TT> & p) 
	{
		resizeJust(p.getsize());
		for (int i=0; i<used; i++) 
		{
			(*this)(i) = (T)(p(i));
		}
	}
	inline pureallocator & operator =(const pureallocator<T> &p) 
	{
		resizeJust(p.getsize());
		for (int i=0; i<used; i++) 
		{
			(*this)(i) = p(i);
		}
		return (*this);
	}
	template <class TT>
	inline void set(const pureallocator<TT> &p) 
	{
		resizeJust(p.getsize());
		for (int i=0; i<used; i++) 
		{
			(*this)(i) = (T)(p(i));
		}
	}
	inline ~pureallocator() { }
	inline int getsize() const { return used; }
	inline void resizeJust(const int newsize) 
	{ 
//		if (newsize>size) 
		{ purevector<T>::resize((int)(newsize)); }
		used = newsize; 
		return;
	}
	inline void resizeJust(const int newsize, const T & initial) 
	{ 
//		if (newsize>size) 
		{ purevector<T>::resize(newsize, initial); }
		used = newsize; 
		return;
	}
	inline void resize(const int newsize) 
	{ 
		if (newsize>purevector<T>::size) 
		{ purevector<T>::resize((int)(newsize*multip_container+add_container)); }
		used = newsize; 
		return;
	}
	inline void resize(const int newsize, const T & initial) 
	{ 
		if (newsize>purevector<T>::size) 
		{ purevector<T>::resize((int)(newsize*multip_container+add_container), initial); }
		used = newsize; 
		return;
	}
	inline void reset() 
	{ resizeJust(0); }
	inline T & add(const T & value) 
	{ 
		resize(getsize()+1);
		return operator()(getsize()-1)=value;
//		return last()=value;
	}
	inline T & add() 
	{ 
		resize(getsize()+1);
		return operator()(getsize()-1);
//		return last();
	}
	inline void erase(int index) 
	{ 
#ifdef _DEBUG
		if (index>=getsize() || index<0) 
			throw "overindexed pureallocator";
#endif
		if (index<getsize()-1)
			purevector<T>::operator()(index)=purevector<T>::operator()(getsize()-1);
//			operator()(index)=last();
		resize(getsize()-1);
	}
	inline void erase() 
	{ 
		resize(max(1,getsize())-1);
	}
	inline const T & last() const 
	{ return operator()(getsize()-1); }
	inline T & last() 
	{ return operator()(getsize()-1); }
};

template <class T>
inline std::ostream & operator<< (std::ostream & os, const pureallocator<T> & v)
{ 
//	indent;
	os << std::endl << "( used : " << v.getsize() << " ,  elem : ";
	{
		////indent;
		os << "  ";
		for (int i=0; i<v.getsize(); i++)
		{
			os << std::endl << "( " << i << " : ";
			{
				////indent;
				os << "  ";
				os << v(i);
			}
			os  << " ) ";
		}
	}
	return os << ") ";
}
template <class T>
inline std::istream & operator>> (std::istream & is, pureallocator<T> & v)
{ 
int size, ind;
	is >> iodummy >> iodummy >> iodummy >> size >> iodummy >> iodummy >> iodummy;
	v.reset();
	v.resizeJust(size);
	for (int i=0; i<v.getsize(); i++)
	{
		is >> iodummy >> ind;
		is >> iodummy >> v(ind) >> iodummy;
	}
	return is >> iodummy;
}

template class pureallocator<double>;
template class pureallocator<ldouble>;
template class pureallocator<vector3<double> >;
template class purevector<pureallocator<vector3<double> > > ;

///////////////////////////////////////////////////////////////////////////////////////////

// MANUAL usifying features of vector (with arithmetics) and allocator (with economic reallocation)
template <class T>
class allocator : public pureallocator<T> 
{
public:
	inline allocator(int _size) : pureallocator<T>(_size) { }
	inline allocator(int _size, const T & initial) : pureallocator<T>(_size, initial) { }
	inline allocator() : pureallocator<T>() { }
	inline allocator(const ptrNsize<T> &p) : pureallocator<T>(p) { }
	inline allocator(const purevector<T> &p) : pureallocator<T>(p) { }
	inline allocator(const pureallocator<T> &p) : pureallocator<T>(p) { }
	inline allocator(const allocator<T> &p) : pureallocator<T>(p) { }
	inline allocator & operator =(const pureallocator<T> & p) 
	{ pureallocator<T>::operator=(p); return *this; }
	template <class TT>
	inline void set(const pureallocator<TT> & p) 
	{ pureallocator<T>::set(p); }
	inline allocator & operator =(const T & initial) 
	{ pureallocator<T>::operator=(initial); return *this; }

	inline allocator & setminus() 
	{ 
		for (int i=0; i<pureallocator<T>::getsize(); i++) 
			pureallocator<T>::elem[i]=-pureallocator<T>::elem[i]; 
		return *this; 
	}
	inline allocator & operator +=(const allocator &p)
	{ 
	int s = min(pureallocator<T>::getsize(),p.getsize()); 
		for (int i=0; i<s; i++) 
			pureallocator<T>::elem[i]+=p[i]; 
		return *this; 
	}
	inline allocator & operator -=(const allocator &p)
	{ 
	int s = min(pureallocator<T>::getsize(),p.getsize()); 
		for (int i=0; i<s; i++) 
			pureallocator<T>::elem[i]-=p[i]; 
		return *this; 
	}
	inline allocator & operator ^=(const allocator &p)
	{ 
	int s = min(pureallocator<T>::getsize(),p.getsize()); 
		for (int i=0; i<s; i++) 
			pureallocator<T>::elem[i]*=p[i]; 
		return *this; 
	}
	inline allocator & operator *=(const T & c) 
	{ 
		for (int i=0; i<pureallocator<T>::getsize(); i++) 
			pureallocator<T>::elem[i]*=c; 
		return *this; 
	}
	inline allocator & operator /=(const T & c) 
	{ 
		for (int i=0; i<pureallocator<T>::getsize(); i++) 
			pureallocator<T>::elem[i]/=c; 
		return *this; 
	}
/*
	inline allocator operator -() const 
	{ 
	allocator ret(*this); 
		return ret.minus(); 
	}
*/
	inline allocator operator +(const allocator &p) const 
	{ 
	allocator ret(*this); 
		ret+=p; 
		return ret; 
	}
	inline allocator operator -(const allocator &p) const 
	{ 
	allocator ret(*this); 
		ret-=p; 
		return ret; 
	}
	inline allocator operator ^(const allocator &p) const 
	{ 
	allocator ret(*this); 
		ret^=p; 
		return ret; 
	}
	inline allocator operator *(const T & c) const 
	{ 
	allocator ret(*this); 
		ret*=c; 
		return ret; 
	}
	inline allocator operator /(const T & c) const 
	{ 
	allocator ret(*this); 
		ret/=c; 
		return ret; 
	}
	inline T operator *(const allocator &p) const
	{ 
	int i, s=min(pureallocator<T>::getsize(),p.getsize()); 
	T ret=0; 
		for (i=0; i<s; i++) ret += pureallocator<T>::elem[i]*p[i]; 
		return ret; 
	}
	inline T norm2 () const 
	{ 
		return (operator*(*this)); 
	}
};

template class allocator<double>;
template class allocator<ldouble>;
//template class allocator<vector3<double> >;
//template class allocator<vector6<double> >;

#endif // __VECTOR_H__
