// MANUAL Basic geometric routines, inline functions, macros, templates, defines - mostly with self explaining names

#ifndef __BASEGEOM_H__
#define __BASEGEOM_H__

#include <iostream>

#include "basics.h"

/////////////////////////////////////////////////////////////////////////////////////////

// MANUAL multidimensional min max macros

#define min1(x) (x)
#define max1(x) (x)

#define min2(x,y) (min((x),(y)))
#define max2(x,y) (max((x),(y)))

#define min3(x,y,z) (min(min((x),(y)),(z)))
#define max3(x,y,z) (max(max((x),(y)),(z)))

// MANUAL determinants
#define det1(a11) \
         (a11)
#define det2(a11, a12, \
			  a21, a22) \
         ((a11)*(a22) \
		 -(a12)*(a21))
#define det2_(T,U) det2(T.x,T.y,U.x,U.y)
#define det3(a11, a12, a13, \
			  a21, a22, a23, \
			  a31, a32, a33 ) \
         ((a11)*det2(a22,a23, \
					a32,a33) \
		 -(a12)*det2(a21,a23, \
					a31,a33) \
		 +(a13)*det2(a21,a22, \
					a31,a32))

#define det3_(T,U,V) det3(T.x,T.y,T.z,U.x,U.y,U.z,V.x,V.y,V.z)

// template <class T> inline double sqr(const T & x) { return sqrt((double)x); }

/////////////////////////////////////////////////////////////////////////////////////////

// MANUAL 2d vector tamplate, with commonsense notofications, global funtions, and  operator>>, operator<< functions
// MANUAL operator* means scalar product, operator^ means coordinate wise product
// MANUAL projectByOrth results in projection on coordsys (ax,ay) 
// MANUAL O(), I(), X(), Y() means:  origin, 1, x, and y vectors, resp
template <class T> class vector2
{
public:
	T x, y;
	inline vector2 () {}
	inline ~vector2 () {}
	template <class TT>
	inline vector2 & set(const vector2 <TT>  &p) 
	{ x= (T) (p.x); y= (T) (p.y); return (*this); }
	inline vector2 (const vector2 &p) { set(p); }
	inline void set(const T & xx, const T & yy) { x=xx; y=yy; }
	inline vector2 (const T & xx, const T & yy) { set(xx, yy); }
	inline vector2 & operator =(const vector2 &p) { return set(p); }
	inline vector2 & operator +=(const vector2 &p) {x+=p.x; y+=p.y; return (*this); }
	inline vector2 operator +(const vector2 &p) const { return (vector2(x+p.x, y+p.y)); }
	inline vector2 & operator -=(const vector2 &p) {x-=p.x; y-=p.y; return (*this); }
	inline vector2 operator -(const vector2 &p) const { return (vector2(x-p.x, y-p.y)); }
//	inline vector2 operator -() const { return (vector2(-x, -y)); }
	inline vector2 & setminus() { x=-x; y=-y; return *this; }
	inline vector2 minus() const { return (vector2(-x, -y)); }
	inline vector2 & operator ^=(const vector2 & p) {x*=p.x; y*=p.y; return (*this); }  
	inline vector2 operator ^(const vector2 & p) const { return (vector2(x*p.x, y*p.y)); } 
	inline vector2 & operator *=(const T & c) {x*=c; y*=c; return (*this); }
	inline vector2 operator *(const T & c) const { return (vector2(x*c, y*c)); }
//	inline vector2 & operator /=(const T & c) {x/=c; y/=c; return (*this); }
//	inline vector2 operator /(const T & c) const { return (vector2(x/c, y/c)); }
	inline vector2 & operator /=(double c) {x/=c; y/=c; return (*this); }
	inline vector2 operator /(double c) const { return (vector2(x/c, y/c)); }
	inline T operator * (const vector2 & p) const { return (x*p.x + y*p.y); } 
	inline bool operator ==(const vector2 & p) const { return (x==p.x) && (y==p.y); }
	inline bool operator !=(const vector2 & p) const { return (! operator==(p)); }
	inline bool operator <(const vector2 & p) const { return (x<p.x) && (y<p.y); }
	inline bool operator <=(const vector2 & p) const { return (x<=p.x) && (y<=p.y); }
	inline bool operator >(const vector2 & p) const { return (x>p.x) && (y>p.y); }
	inline bool operator >=(const vector2 & p) const { return (x>=p.x) && (y>=p.y); }
	inline vector2 projectByOrth (const vector2 & ax, const vector2 & ay) const { return vector2(*this*ax,*this*ay); }
	inline void projectByOrth (vector2 & ret, const vector2 & ax, const vector2 & ay) const { ret.set(*this*ax,*this*ay); }
	inline T norm2 () const { return (operator*(*this)); }
	inline T maxnorm () const { return max(abs(x),abs(y)); }
	inline vector2 normalized() const 
	{ 
		double n=(double)norm2(); 
		return (n<EPS?vector2( (T) 0, (T) 0):*this/ (T) sqrt(n)); 
	}
	inline T cMin() { return min(x,y); }
	inline T cMax() { return max(x,y); }
	static vector2 O() { return vector2(0,0); }
	static vector2 I() { return vector2(1,1); }
	static vector2 X() { return vector2(1,0); }
	static vector2 Y() { return vector2(0,1); }
};

template <class T> inline std::ostream & operator<< (std::ostream & os, const vector2<T> & v) 
{ return os << "( " << v.x << " , " << v.y << " ) "; }
template <class T> inline std::istream & operator>> (std::istream & is, vector2<T> & v) 
{ return is >> iodummy >> v.x >> iodummy >> v.y >> iodummy; }

template <class T> inline vector2 <T>  cMin (const vector2 <T>  & p, const vector2 <T>  & q) { return (vector2 <T>  (min(p.x,q.x), min(p.y,q.y))); }
template <class T> inline vector2 <T>  cMax (const vector2 <T>  & p, const vector2 <T>  & q) { return (vector2 <T>  (max(p.x,q.x), max(p.y,q.y))); }
template <class T> inline vector2 <T>  cAbs (const vector2 <T>  & p) { return (vector2 <T>  (abs(p.x), abs(p.y))); }
template <class T> inline vector2 <int>  cSgn (const vector2 <T>  & p) { return (vector2 <int>  (sgn(p.x), sgn(p.y))); }
template <class T> inline vector2 <T>  operator *(const T & p, const vector2 <T>  & q) { return q*p; }

template class vector2<double>;
// template class vector2<vector2<double> >;

/////////////////////////////////////////////////////////////////////////////////////////

// MANUAL 3d vector tamplate, like vector2
template <class T>
class vector3
{
public:
	T x, y, z;
	inline vector3 () {}
	inline ~vector3 () {}
	template <class TT>
	inline vector3 & set(const vector3 <TT>  &p) { x= (T) (p.x); y= (T) (p.y); z= (T) (p.z); return (*this); }
	inline vector3 (const vector3 &p) { set(p); }
	inline void set(const T & xx, const T & yy, const T & zz) { x=xx; y=yy; z=zz; }
	inline vector3 (const T & xx, const T & yy, const T & zz) { set(xx, yy, zz); }
	inline vector3 & operator =(const vector3 &p) { set(p); return (*this); }
	inline vector3 & operator +=(const vector3 &p) {x+=p.x; y+=p.y; z+=p.z; return (*this); }
	inline vector3 operator +(const vector3 &p) const { return (vector3(x+p.x, y+p.y, z+p.z)); }
	inline vector3 & operator -=(const vector3 &p) {x-=p.x; y-=p.y; z-=p.z; return (*this); }
	inline vector3 operator -(const vector3 &p) const { return (vector3(x-p.x, y-p.y, z-p.z)); }
//	inline vector3 operator -() const { return (vector3(-x, -y, -z)); }
	inline vector3 & setminus() { x=-x; y=-y; z=-z; return *this; }
	inline vector3 minus() const { return (vector3(-x, -y, -z)); }
	inline vector3 & operator ^=(const vector3 & p) {x*=p.x; y*=p.y; z*=p.z; return (*this); }
	inline vector3 operator ^(const vector3 & p) const { return (vector3(x*p.x, y*p.y, z*p.z)); }
	inline vector3 & operator *=(const T & c) {x*=c; y*=c; z*=c; return (*this); }
	inline vector3 operator *(const T & c) const { return (vector3(x*c, y*c, z*c)); }
//	inline vector3 & operator /=(const T & c) {x/=c; y/=c; z/=c; return (*this); }
//	inline vector3 operator /(const T & c) const { return (vector3(x/c, y/c, z/c)); }
	inline vector3 & operator /=(double c) {x/=c; y/=c; z/=c; return (*this); }
	inline vector3 operator /(double c) const { return (vector3(x/c, y/c, z/c)); }
	inline T operator * (const vector3 & p) const { return (x*p.x + y*p.y + z*p.z); }
	inline bool operator ==(const vector3 & p) const { return (x==p.x) && (y==p.y) && (z==p.z); }
	inline bool operator !=(const vector3 & p) const { return (! operator==(p)); }
	inline bool operator <(const vector3 & p) const { return (x<p.x) && (y<p.y) && (z<p.z); }
	inline bool operator <=(const vector3 & p) const { return (x<=p.x) && (y<=p.y) && (z<=p.z); }
	inline bool operator >(const vector3 & p) const { return (x>p.x) && (y>p.y) && (z>p.z); }
	inline bool operator >=(const vector3 & p) const { return (x>=p.x) && (y>=p.y) && (z>=p.z); }
	inline vector3 projectByOrth (const vector3 & ax, const vector3 & ay, const vector3 & az) const 
	{ return vector3(*this*ax,*this*ay,*this*az); }
	inline void projectByOrth (vector3 & ret, const vector3 & ax, const vector3 & ay, const vector3 & az) const 
	{ ret.set(*this*ax,*this*ay,*this*az); }
	inline T norm2 () const { return (operator*(*this)); }
	inline T maxnorm () const { return max3(abs(x),abs(y),abs(z)); }
	inline vector3 normalized() const 
	{ 
		double n=sqrt((double)norm2()); //T nn = (T)sqrt(n); 
		return (n<EPS?vector3( (T) 0, (T) 0, (T) 0):*this/n); 
	}
//	inline vector3 normalized() const { double n=(double)norm2(); return (n<EPS?vector3( (T) 0, (T) 0, (T) 0):*this/ (T) sqrt(n)); }
//	inline vector3 normalized() const { double n=(double)norm2(); 
//		if (n<EPS) return vector3( (T) 0, (T) 0, (T) 0);
//		else return *this/ (T) sqrt(n); 
//	}
	inline vector3 operator %(const vector3 & q) const 
	{
		return (vector3 (det2(y, z, 
							q.y, q.z), 
						-det2(x, z, 
							q.x, q.z), 
						det2(x, y, 
							q.x, q.y)));
	}
	inline vector3 & operator %=(const vector3 & q) { return (*this=*this%q); }
	inline T cMin() { return min3(x,y,z); }
	inline T cMax() { return max3(x,y,z); }
	static vector3 O() { return vector3(0,0,0); }
	static vector3 I() { return vector3(1,1,1); }
	static vector3 X() { return vector3(1,0,0); }
	static vector3 Y() { return vector3(0,1,0); }
	static vector3 Z() { return vector3(0,0,1); }
};

template <class T> inline std::ostream & operator<<(std::ostream & os, const vector3<T> & v) 
{ return os << "( " << v.x << " , " << v.y << " , " << v.z << " ) "; }
template <class T> inline std::istream & operator>> (std::istream & is, vector3<T> & v) 
{ return is >> iodummy >> v.x >> iodummy >> v.y >> iodummy >> v.z >> iodummy; }

template <class T> inline vector3 <T>  cMin (const vector3 <T>  & p, const vector3 <T>  & q) { return (vector3 <T>  (min(p.x,q.x), min(p.y,q.y), min(p.z,q.z))); }
template <class T> inline vector3 <T>  cMax (const vector3 <T>  & p, const vector3 <T>  & q) { return (vector3 <T>  (max(p.x,q.x), max(p.y,q.y), max(p.z,q.z))); }
template <class T> inline vector3 <T>  cAbs (const vector3 <T>  & p) { return (vector3 <T>  (abs(p.x), abs(p.y), abs(p.z))); }
template <class T> inline vector3 <int>  cSgn (const vector3 <T>  & p) { return (vector3 <int>  (sgn(p.x), sgn(p.y), sgn(p.z))); }
template <class T> inline vector3 <T>  operator*(const T & p, const vector3 <T>  & q) { return q*p; }

template class vector3<double>;
// template class vector3<vector3<double> >;

/////////////////////////////////////////////////////////////////////////////////////////

#endif // __BASEGEOM_H__
