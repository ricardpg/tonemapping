/*
  Autor(s)      : Jordi Ferrer Plana
  e-mail        : jferrerp@eia.udg.edu
  Branch        : Computer Vision

  Working Group : Underwater Vision Lab
  Project       : Correlation Functions
  Homepage      : http://uvl.udg.edu

  Module        : Generic error functions.

  File          : errorMx.h
  Date          : 11/02/2008 - 11/02/2008
  Encoding      : ISO-8859-1 (Latin-1)

  Compiler      : MATLAB >= 7.0 & g++ >= 4.x
  Libraries     :

  Notes         :

 -----------------------------------------------------------------------------

  Copyright (C) 2008 by Jordi Ferrer Plana

  This source code is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This source code is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
  more details.

 -----------------------------------------------------------------------------
*/

#ifndef __ERRORMX_H__
#define __ERRORMX_H__

#define STR_ERROR_BUFFER_SIZE 4096   // Should be enought

//! Function to print mex error using printf() syntax.
//
void printfmexErrMsgTxt ( char *Format, ... );

//! Function to print mex warning using printf() syntax.
//
void printfmexWarnMsgTxt ( char *Format, ... );

#endif
